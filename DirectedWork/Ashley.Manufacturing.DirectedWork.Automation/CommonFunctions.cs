﻿using Ashley.Manufacturing.DirectedWork.Automation.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using QA.AutomationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ashley.Manufacturing.DirectedWork.Automation
{
    class CommonFunctions
    {
        public static object btn_logout = new { Class = "logOutbtn" };
        public static void LaunchBrowser(string Url)
        {
            WebArchive.LaunchBrowser(Url, Browser.Chrome);
            
        }
        public static string Login(string userName, string password)
        {

            string result = string.Empty;
            #region Local Variables

            IWebElement elm_txt_userName = null;
            IWebElement elm_txt_password = null;
            IWebElement elm_btn_login = null;

            #endregion

            #region Steps

            try
            {
                elm_txt_userName = LoginPage.txt_userName.Init();
                elm_txt_userName.EnterText(userName);

                elm_txt_password = LoginPage.txt_password.Init();
                elm_txt_password.EnterText(password);
               
                elm_btn_login = LoginPage.btn_login.Init();
                elm_btn_login.ClickControl();

                WebArchive.WaitForPageLoad();

            }
            catch (Exception ex)
            {
                result = "Error Message :" + ex.Message + ex.StackTrace;
            }

            return result;

            #endregion

        }

        public static string operatorSiteZoneWorkCenterGroupSelection(string site, string zone, string workCenter, string group)
        {

            string result = string.Empty;
            try
            {
                result = OperatorSitesPage.ChooseOperatorSite(site);
                Assert.IsFalse(result.Contains("Error"));

                result =  OperatorZonePage.ChooseZone(zone);
                Assert.IsFalse(result.Contains("Error"));
                result =  OperatorWCenterPage.ChooseWorkCenter(workCenter);
                Assert.IsFalse(result.Contains("Error"));
                result = OperatorGroupPage.ChooseOperatorGroup(group);
                Assert.IsFalse(result.Contains("Error"));


            }
            catch (Exception ex)
            {
                result = "Error Message :" + ex.Message + ex.StackTrace;

            }
            return result;


        }

        public static void Wait(string browser)
        {

            if (browser == "Chrome") { }

            else if (browser == "InternetExplorer")
            {
                WebArchive.WaitForPageLoad(8000, true);
            }
        }

        //public static void WaitForElementLoad(By by, int timeoutInSeconds)
        //{
        //    if (timeoutInSeconds > 0)
        //    {
        //        IWebDriver webDriver = null;
        //        WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(timeoutInSeconds));
        //        wait.Until(ExpectedConditions.ElementIsVisible(by));
        //    }
        //}

        public static void LogOut()
        {
            IWebElement elm_btn_logout = btn_logout.Init();
            Thread.Sleep(1000);
            elm_btn_logout.Click();
            
        }
    }
}
