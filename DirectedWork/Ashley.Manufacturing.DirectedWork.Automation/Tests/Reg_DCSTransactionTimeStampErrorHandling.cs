﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QA.AutomationLibrary;
using Ashley.Manufacturing.DirectedWork.Automation.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Linq;

namespace Ashley.Manufacturing.DirectedWork.Automation.Tests
{
    [TestClass]
    [DeploymentItem(@"TestDriver", @"TestDriver")]
    public class Reg_DCSTransactionTimeStampErrorHandling
    {
        string browser = Automation.Properties.TestData.Default.browser;
        Dictionary<string, IWebDriver> driversByHandles;


        [TestMethod]
        [WorkItem(118164)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(1)]
        [Description("Check users are allowed to 20 transactions using single / Multiple orders after doing 30 Transaction")]
        public void CheckUsersAreAllowedTo20TransactionsUsingSingle_MultipleOrdersAfterDoing30Transaction()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;
            IWebElement elm_btn_20SetUpTimeOn = null;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_btn_35WandOff = null;
            List<IWebElement> elm_row = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["Password"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button  
            CommonFunctions.LaunchBrowser(url);
            WebArchive.WriteLog("Browser Launched");
            

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button

            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);
            WebArchive.WriteLog("Operator Login successful" + status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            WebArchive.WriteLog("Site, Zone, Work center and Group Selected for the Operator" + status);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
           

            // 1.9, Search @Order(MO) from upper grid
            // 1.10, Click over the order searched
            status = PriorityListPage.EnterMO(MO);
            WebArchive.WriteLog("MO Selected" + status);            
            CommonFunctions.Wait(browser);
           

            // 1.11, Click on '20 Setup Time On' button
            elm_btn_20SetUpTimeOn = PriorityListPage.btn_20SetupTimeOn.Init();
            elm_btn_20SetUpTimeOn.ClickControl();
            CommonFunctions.Wait(browser);
            WebArchive.WriteLog("Clicked On 20 Set Up Time On Button");

            // 1.12, Button Status are
            // Enabled button status           
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 1.13, Check 'Group#' is blinking
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");
            // Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetCssValue("animation") == "blinker 1s linear 0s infinite normal none running", "Group Id is not blinking after generating the order");
            elm_row = PriorityListPage.row_zeroBox1.Init().GetChildren();
            Assert.IsTrue(elm_row[1].GetAttribute("class").Contains("yellow"), "MO not in yellow color after 20");

            WebArchive.WriteLog("Verification done for Button status and Group blink after 20 Transaction" );

            // 2, Select @Order1 (MO) from upper grid
            status = PriorityListPage.EnterMO(MO);
            WebArchive.WriteLog("Another MO selected" + status);
            CommonFunctions.Wait(browser);
           

            // 3, Click on 'Wand On Labor (30)' button
            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);
            WebArchive.WriteLog("Clicked On 30 Wand On Button");

            // 4, Enter @QTY @SCRAPQTY @SCRAPCDE for all the listed orders and click on OK button
            // 5, click on 'OK' button
            status = PriorityListPage.SetupTimeOff25(qty, scrapqty, scrapcode);            
            WebArchive.WriteLog("Qty, Scrap qty Entered" + status);

            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            // 6, Enter @RFID1 for each listed orders
            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);
            CommonFunctions.Wait(browser);
            

            // 7, Click on 'OK' button

            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);
            WebArchive.WriteLog("RFID Entered and OK Button Clicked" + status);

            // 8, Check the status of 'Group#'           
            //var sa = PriorityListPage.spn_GroupId.Init().GetAttribute("class");
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");
            elm_row = PriorityListPage.row_zeroBox1.Init().GetChildren();
            Assert.IsTrue(elm_row[1].GetAttribute("class").Contains("green"), "MO not in green color after 30");

            // 9, Check the status of buttons
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            WebArchive.WriteLog("Verification done for Buttons after 30 transaction" + status);

            // 10, Select 35 button
            elm_btn_35WandOff = PriorityListPage.btn_35WandOff.Init();
            elm_btn_35WandOff.ClickControl();
            CommonFunctions.Wait(browser);

            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            WebArchive.WriteLog("Pallete complete Selected and Ok clicked" + status);
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            
            #endregion

            #region Test Cleanup



            #endregion

        }

        [TestMethod]
        [WorkItem(118166)]
        [Description("Check users are allowed to 50 transaction after doing same 30 Transaction ")]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(1)]
        //  [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\Data1.csv", "Data#csv", DataAccessMethod.Sequential)]
        public void CheckUsersAreAllowedTo50TransactionAfterDoingSame30Transaction()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;
            IWebElement elm_btn_50IndirectDtOn = null;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_btn_35WandOff = null;
            IWebElement elm_btn_downTimeOff = null;
            List<IWebElement> elm_row = null;


            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string downTime = Automation.Properties.TestData.Default.DTreason;

            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button  
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button
            status = CommonFunctions.Login(userName, password);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 1.9, Click on '50 Indirect/DT On ' button
            elm_btn_50IndirectDtOn = PriorityListPage.btn_50IndirectDtOn.Init();
            elm_btn_50IndirectDtOn.ClickControl();
            CommonFunctions.Wait(browser);


            // 1.10, Select DownTime Code and click on OK button
            status = PriorityListPage.DownTimeOnReason(downTime);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 1.11, Button Status are
            // Enabled button status           
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");


            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // 1.11, Check 'Group#' is blinking
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");

            // 2, Select @Order1 (MO) from upper grid
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 3, Click on 'Wand On Labor (30)' button
            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            elm_btn_downTimeOff = PriorityListPage.btn_okBtn.Init();
            elm_btn_downTimeOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 4, Enter @RFID1 for  listed order
            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            // 5, Click on 'OK' button
            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // 6, Check the status of 'Group#'         
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");
            elm_row = PriorityListPage.row_zeroBox1.Init().GetChildren();
            Assert.IsTrue(elm_row[1].GetAttribute("class").Contains("green"), "MO not in green color after 20");

            // 1.17, Check the status of buttons
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // 1.18, Select 35 button
            elm_btn_35WandOff = PriorityListPage.btn_35WandOff.Init();
            elm_btn_35WandOff.ClickControl();
            CommonFunctions.Wait(browser);

            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }
            #endregion

            #region Test Cleanup



            #endregion

        }

        [TestMethod]
        [WorkItem(118177)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(1)]
        [Description("Check users are allowed to 30 transaction using single / Multiple orders after doing 20 Transaction")]
        public void CheckUsersAreAllowedTo30TransactionUsingSingle_MultipleOrdersAfterDoing20Transaction()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;
            IWebElement elm_btn_20SetUpTimeOn = null;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_btn_25setUpOff = null;
            List<IWebElement> elm_row = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button

            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 1.9, Check all the button mode
            // Enabled button status           
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 1.10, Search @Order(MO) from upper grid
            // 1.11, Click over the order searched
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            Assert.IsTrue(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // 1.12, Click on 'Wand On Labor (30)' button
            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 1.13, Enter @RFID and click on button 'OK'
            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // Enabled button status           
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 1.14, Check 'Group#' is blinking
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");
            elm_row = PriorityListPage.row_zeroBox1.Init().GetChildren();
            Assert.IsTrue(elm_row[1].GetAttribute("class").Contains("green"), "MO not in green color after 30");

            // 2, Select @Order1 (MO) from upper grid
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);

            // 3, Click on '20 Set Up Time On ' button
            elm_btn_20SetUpTimeOn = PriorityListPage.btn_20SetupTimeOn.Init();
            elm_btn_20SetUpTimeOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 4, Enter @QTY @SCRAPECODE @PALLETCOMPLETE for all the listed orders and click on OK button
            // 5, click on 'OK' button
            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            // 6, Check the status of 'Group#'
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");
            elm_row = PriorityListPage.row_zeroBox1.Init().GetChildren();
            Assert.IsTrue(elm_row[1].GetAttribute("class").Contains("yellow"), "MO not in yellow color after 20");

            // 7 ,Check the status of buttons
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // 8, Select 25 button
            elm_btn_25setUpOff = PriorityListPage.btn_25SetupTimeOff.Init();
            elm_btn_25setUpOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 9, Select Ok
            status = PriorityListPage.SetupTimeOff25(qty, scrapqty, scrapcode);
            Assert.IsFalse(status.Contains("Error"), status);

            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            #endregion

            #region Test Cleanup

            #endregion


        }

        [TestMethod]
        [WorkItem(118179)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(1)]
        [Description("Check users are allowed to 50 transaction using single / Multiple orders after doing 20 Transaction")]
        public void CheckUsersAreAllowedTo50TransactionUsingSingle_MultipleOrdersAfterDoing20Transaction()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_55DownTimeOff = null;
            IWebElement elm_btn_20SetUpTimeOn = null;
            IWebElement elm_btn_50DownTimwOn = null;
            IWebElement elm_btn_25setUpOff = null;
            List<IWebElement> elm_row = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string reason = Automation.Properties.TestData.Default.DTreason;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);


            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button

            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.9, Click on '50 Indirect/DT On' button
            elm_btn_50DownTimwOn = PriorityListPage.btn_50IndirectDtOn.Init();
            elm_btn_50DownTimwOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 1.10, Select @DownTimeCode
            // 1.11, Click on button 'OK' button with/without comments
            PriorityListPage.DownTimeOnReason(reason);

            // 1.12, Button Status are
            // Enabled button status           
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 1.13, Check 'Group#' is blinking
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");

            // 2, Select @Order1 (MO) from upper grid
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);

            Assert.IsTrue(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // 3, Click on '20 Set Up Time On ' button
            elm_btn_20SetUpTimeOn = PriorityListPage.btn_20SetupTimeOn.Init();
            elm_btn_20SetUpTimeOn.ClickControl();
            CommonFunctions.Wait(browser);


            // 4, Enter @DownTimeCode @Comments and click on OK button
            elm_btn_55DownTimeOff = PriorityListPage.btn_downTimeOff.Init();
            elm_btn_55DownTimeOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 5, Check the status of 'Group#'
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");
            elm_row = PriorityListPage.row_zeroBox1.Init().GetChildren();
            Assert.IsTrue(elm_row[1].GetAttribute("class").Contains("yellow"), "MO not in yellow color after 20");

            // 6, Check the status of buttons
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // 7, Select 25 button
            elm_btn_25setUpOff = PriorityListPage.btn_25SetupTimeOff.Init();
            elm_btn_25setUpOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 8, Select ok
            status = PriorityListPage.SetupTimeOff25(qty, scrapqty, scrapcode);
            Assert.IsFalse(status.Contains("Error"), status);

            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            #endregion

            #region Test Cleanup

            #endregion


        }

        [TestMethod]
        [WorkItem(118180)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(1)]
        [Description("Check users are allowed to 20 transaction using single / Multiple orders after doing 50 Transaction")]
        public void CheckUsersAreAllowedTo20TransactionUsingSingle_MultipleOrdersAfterDoing50Transaction()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_55DTOff = null;
            IWebElement elm_btn_55DownTimeOff = null;
            IWebElement elm_btn_20SetUpTimeOn = null;
            IWebElement elm_btn_50IndirectDtOn = null;
            List<IWebElement> elm_row = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string reason = Automation.Properties.TestData.Default.DTreason;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);


            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 1.9, Search @Order(MO) from upper grid
            // 1.10, Click over the order searched
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 1.11, Click on '20 Setup Time On' button
            elm_btn_20SetUpTimeOn = PriorityListPage.btn_20SetupTimeOn.Init();
            elm_btn_20SetUpTimeOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 1.12, Button Status are
            // Enabled button status           
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 1.13, Check 'Group#' is blinking
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");
            // Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetCssValue("animation") == "blinker 1s linear 0s infinite normal none running", "Group Id is not blinking after generating the order");
            elm_row = PriorityListPage.row_zeroBox1.Init().GetChildren();
            Assert.IsTrue(elm_row[1].GetAttribute("class").Contains("yellow"), "MO not in yellow color after 20");

            // 2, Click on '50 Indirect/DT On ' button
            elm_btn_50IndirectDtOn = PriorityListPage.btn_50IndirectDtOn.Init();
            elm_btn_50IndirectDtOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 3, Enter @QTY @SCRAPQTY @SCRAPCDE for all the listed orders and click on OK button
            // 4, click on 'OK' button
            status = PriorityListPage.SetupTimeOff25(qty, scrapqty, scrapcode);
            Assert.IsFalse(status.Contains("Error"), status);

            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
            }

            // 5, Select DownTime Code
            // 6, Click on button 'OK' button with/without comments
            status = PriorityListPage.DownTimeOnReason(reason);
            Assert.IsFalse(status.Contains("Error"), status);

            // 7, Select 55 Indirect DT off
            elm_55DTOff = PriorityListPage.btn_55IndirectDtOff.Init();
            elm_55DTOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 8, Select ok
            elm_btn_55DownTimeOff = PriorityListPage.btn_downTimeOff.Init();
            elm_btn_55DownTimeOff.ClickControl();
            CommonFunctions.Wait(browser);
            #endregion

            #region Test Cleanup

            #endregion

        }

        [TestMethod]
        [WorkItem(118181)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(1)]
        [Description("Check users are allowed to 30 transaction using single / Multiple orders after doing 50 Transaction")]
        public void CheckUsersAreAllowedTo30TransactionUsingSingle_MultipleOrdersAfterDoing50Transaction()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_55DTOff = null;
            IWebElement elm_btn_55DownTimeOff = null;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_50IndirectDtOn = null;
            IWebElement elm_btn_RfidOk = null;
            List<IWebElement> elm_row = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string reason = Automation.Properties.TestData.Default.DTreason;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button

            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 1.9, Check all the button mode
            // Enabled button status           
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 1.10, Search @Order(MO) from upper grid
            // 1.11, Click over the order searched
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 1.12, Click on 'Wand On Labor (30)' button
            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 1.13, Enter @RFID and click on button 'OK'
            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // Enabled button status           
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 1.14, Check 'Group#' is blinking
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");
            elm_row = PriorityListPage.row_zeroBox1.Init().GetChildren();
            Assert.IsTrue(elm_row[1].GetAttribute("class").Contains("green"), "MO not in green color after 30");


            // 2, Click on '50 Indirect/DT On ' button
            elm_btn_50IndirectDtOn = PriorityListPage.btn_50IndirectDtOn.Init();
            elm_btn_50IndirectDtOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 3, Enter @QTY @SCRAPQTY @SCRAPCDE for all the listed orders and click on OK button
            // 4, click on 'OK' button
            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);

            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            // 5, Select DownTime Code
            // 6, Click on button 'OK' button with/without comments
            status = PriorityListPage.DownTimeOnReason(reason);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 7, Check 'Group#' is blinking
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");

            // 8, Chexk the status of the group
            // Enabled button status           
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // 9, Select 55 Indirect DT off
            elm_55DTOff = PriorityListPage.btn_55IndirectDtOff.Init();
            elm_55DTOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 10, Select ok
            elm_btn_55DownTimeOff = PriorityListPage.btn_downTimeOff.Init();
            elm_btn_55DownTimeOff.ClickControl();
            CommonFunctions.Wait(browser);
            #endregion

            #region Test Cleanup

            #endregion

        }

        [TestMethod]
        [WorkItem(118548)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(7)]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "C:\\ToWork\\Ashley\\QA\\DirectedWork\\Ashley.Manufacturing.DirectedWork.Automation\\TestData\\Data.csv", "Data#csv", DataAccessMethod.Sequential)]
        [Description("Validate 'RFID' fields using valid and invalid values")]
        public void ValidateRFIDFieldsUsingValidAndInvalidValues()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_35WandOff = null;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            //string password = Automation.Properties.TestData.Default.password;
            //string site = Automation.Properties.TestData.Default.site;
            //string zone = Automation.Properties.TestData.Default.zone;
            //string workCenter = Automation.Properties.TestData.Default.workcenter;
            //string group = Automation.Properties.TestData.Default.group;
            //string url = Automation.Properties.TestData.Default.Url;
            //string MO = Automation.Properties.TestData.Default.mo;
            //string qty = Automation.Properties.TestData.Default.qty;
            //string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            //string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            //string RFID = Automation.Properties.TestData.Default.RFID;
            //string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string reason = Automation.Properties.TestData.Default.DTreason;
            string site = TestContext.DataRow["site"].ToString();
            string password = TestContext.DataRow["password"].ToString();
            //   string UserName = TestContext.DataRow["username"].ToString();
           
            // string password = Automation.Properties.TestData.Default.passwordTestContext.DataRow["password"].ToString();
            //  string site = Automation.Properties.TestData.Default.site;
            string zone = /*Automation.Properties.TestData.Default.zone*/TestContext.DataRow["zone"].ToString();
            string workCenter = /*Automation.Properties.TestData.Default.workcenter*/TestContext.DataRow["workcenter"].ToString();
            string group = /*Automation.Properties.TestData.Default.group*/TestContext.DataRow["group"].ToString();
            string url = Automation.Properties.TestData.Default.Url;
            string MO = /*Automation.Properties.TestData.Default.mo*/TestContext.DataRow["mo"].ToString();
            string qty = /*"1000"*/TestContext.DataRow["qty"].ToString();
            //string scrapqty = /*Automation.Properties.TestData.Default.scrapqty*/TestContext.DataRow["scrapqty"].ToString();
            string scrapcode = "OS  ";
            string RFID = /*Automation.Properties.TestData.Default.RFID*/TestContext.DataRow["rfid"].ToString();
            string palletcomplete = /*Automation.Properties.TestData.Default.palletcomplete*/TestContext.DataRow["palletcomplete"].ToString();
            //string reason = Automation.Properties.TestData.Default.DTreason;
            //string palletcomplete1 = Automation.Properties.TestData.Default.palletComplete1;
            //string scrapcode1 = Automation.Properties.TestData.Default.scrapCode1;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 2, Select @Order (MO) from upper grid
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 3, Click on 'Wand On Labor (30)' button
            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 4, Enter @InvalidRfid
            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText("32f22r3");

            // 5, Click ok button
            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.Click();
            CommonFunctions.Wait(browser);

            // 6, Click ok button
            var alertText = GlobalProperties.driver.SwitchTo().Alert().Text;
            Assert.AreEqual("Please enter valid RFID", GlobalProperties.driver.SwitchTo().Alert().Text);
            GlobalProperties.driver.SwitchTo().Alert().Accept();

            // 7, Enter @ValidRFID
            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            // 8, Click ok button
            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // 9, Select 35 button
            elm_35WandOff = PriorityListPage.btn_35WandOff.Init();
            elm_35WandOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 10, Select Pallet Complete
            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);

            // 11, Select Ok
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            #endregion

            #region Test Cleanup

            #endregion


        }

        [TestMethod]
        [WorkItem(115254)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(7)]
        [Description("Valide the fields in the 50 and 55 screens")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "C:\\ToWork\\Ashley\\QA\\DirectedWork\\Ashley.Manufacturing.DirectedWork.Automation\\TestData\\Data.csv", "Data#csv", DataAccessMethod.Sequential)]
        public void ValideTheFieldsInThe50And55Screens()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_55DTOff = null;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;
            IWebElement elm_btn_50IndirectDtOn = null;
            IWebElement elm_btn_reasonCodeOk = null;
            IWebElement elm_btn_55DownTimeOff = null;
            List<IWebElement> elm_row = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            //string userName = Automation.Properties.TestData.Default.username;
            //string password = Automation.Properties.TestData.Default.password;
            //string site = Automation.Properties.TestData.Default.site;
            //string zone = Automation.Properties.TestData.Default.zone;
            //string workCenter = Automation.Properties.TestData.Default.workcenter;
            //string group = Automation.Properties.TestData.Default.group;
            //string url = Automation.Properties.TestData.Default.Url;
            //string MO = Automation.Properties.TestData.Default.mo;
            //string qty = Automation.Properties.TestData.Default.qty;
            //string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            //string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            //string RFID = Automation.Properties.TestData.Default.RFID;
            //string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;

            string site = TestContext.DataRow["site"].ToString();
            string password = TestContext.DataRow["password"].ToString();
            //   string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            // string password = Automation.Properties.TestData.Default.passwordTestContext.DataRow["password"].ToString();
            //  string site = Automation.Properties.TestData.Default.site;
            string zone = /*Automation.Properties.TestData.Default.zone*/TestContext.DataRow["zone"].ToString();
            string workCenter = /*Automation.Properties.TestData.Default.workcenter*/TestContext.DataRow["workcenter"].ToString();
            string group = /*Automation.Properties.TestData.Default.group*/TestContext.DataRow["group"].ToString();
            string url = Automation.Properties.TestData.Default.Url;
            string MO = /*Automation.Properties.TestData.Default.mo*/TestContext.DataRow["mo"].ToString();
            string qty = /*"1000"*/TestContext.DataRow["qty"].ToString();
            //string scrapqty = /*Automation.Properties.TestData.Default.scrapqty*/TestContext.DataRow["scrapqty"].ToString();
            string scrapcode = "OS  ";
            string RFID = /*Automation.Properties.TestData.Default.RFID*/TestContext.DataRow["rfid"].ToString();
            string palletcomplete = /*Automation.Properties.TestData.Default.palletcomplete*/TestContext.DataRow["palletcomplete"].ToString();
            //string reason = Automation.Properties.TestData.Default.DTreason;
            //string palletcomplete1 = Automation.Properties.TestData.Default.palletComplete1;
            //string scrapcode1 = Automation.Properties.TestData.Default.scrapCode1;
            string reason = Automation.Properties.TestData.Default.DTreason;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 2, Observe the Priority List Screen
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // Pre-Requisite
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // 3, Observe the lower grid
            elm_row = PriorityListPage.row_zeroBox1.Init().GetChildren();
            Assert.IsTrue(elm_row[1].GetAttribute("class").Contains("green"), "MO not in green color after 30");
            WebArchive.WaitForPageLoad();

            // 4, Select any order from the upper grid
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 5, Select 50 button
            elm_btn_50IndirectDtOn = PriorityListPage.btn_50IndirectDtOn.Init();
            elm_btn_50IndirectDtOn.ClickControl();
            CommonFunctions.Wait(browser);

            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);

            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            // 6, select ok
            Thread.Sleep(5000);
            elm_btn_reasonCodeOk = PriorityListPage.btn_downtimeOk.Init();
            elm_btn_reasonCodeOk.Click();

            var alertText = GlobalProperties.driver.SwitchTo().Alert().Text;            
            Assert.AreEqual("Please select valid downtime code", GlobalProperties.driver.SwitchTo().Alert().Text);
            GlobalProperties.driver.SwitchTo().Alert().Accept();

            // 7, Select 50 and press enter
            status = PriorityListPage.DownTimeOnReason(reason);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 8, Select 55           
            elm_55DTOff = PriorityListPage.btn_55IndirectDtOff.Init();
            elm_55DTOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 9, select ok
            // 10, select code and press enter
            elm_btn_55DownTimeOff = PriorityListPage.btn_downTimeOff.Init();
            elm_btn_55DownTimeOff.ClickControl();
            CommonFunctions.Wait(browser);
            #endregion

            #region Test Cleanup

            #endregion


        }

        [TestMethod]
        [WorkItem(114977)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(3)]
        [Description(" Validate the fields while doing 35 for an order which 30 is already done")]
        public void ValidateTheFieldsWhileDoing35ForAnOrderWhich30IsAlreadyDone()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;
            List<IWebElement> elm_row = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string reason = Automation.Properties.TestData.Default.DTreason;
            string palletcomplete1 = Automation.Properties.TestData.Default.palletComplete1;
            string scrapcode1 = Automation.Properties.TestData.Default.scrapCode1;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();

            // 2, Observe the Priority List Screen
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // Pre-Requisite
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // 3, Observe the lower grid
            elm_row = PriorityListPage.row_zeroBox1.Init().GetChildren();
            Assert.IsTrue(elm_row[1].GetAttribute("class").Contains("green"), "MO not in green color after 30");
            WebArchive.WaitForPageLoad();

            // 4, Select any order from the upper grid
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 5, Select Wand On Labor 30 button
            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 6, Verify the 35 screen
            // 7, Select Ok without filling any field
            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete1);
            var alertText = GlobalProperties.driver.SwitchTo().Alert().Text;
            Assert.AreEqual("Please select pallet complete value", "Please select pallet complete value");
            GlobalProperties.driver.SwitchTo().Alert().Accept();

            // 8, Select Qty and Enter Scrap code as Rw
            status = PriorityListPage.WandOff35(qty, scrapcode1, palletcomplete);
            // 9, Select Ok in the Hard error
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            // 10, Enter Valid Scrap code or leave blank
            // 11, Select pallet complete from drop down and click on "OK" button
            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);

            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }
            #endregion

            #region Test Cleanup

            #endregion
        }

        [TestMethod]
        [WorkItem(114932)]
        [TestCategory("csv")]
        [Priority(7)]
        [Description(" Verify the alert message when user enters Qty within range in the wand off screen")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "C:\\ToWork\\Ashley\\QA\\DirectedWork\\Ashley.Manufacturing.DirectedWork.Automation\\TestData\\Data.csv", "Data#csv", DataAccessMethod.Sequential)]
        public void VerifyTheAlertMessageWhenUserEntersQtyWithinRangeInTheWandOffScreen()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;
            List<IWebElement> elm_row = null;

            #endregion

            #region Test Data
            string site = TestContext.DataRow["site"].ToString();
            string password = TestContext.DataRow["password"].ToString();
            //   string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            // string password = Automation.Properties.TestData.Default.passwordTestContext.DataRow["password"].ToString();
            //  string site = Automation.Properties.TestData.Default.site;
            string zone = /*Automation.Properties.TestData.Default.zone*/TestContext.DataRow["zone"].ToString();
            string workCenter = /*Automation.Properties.TestData.Default.workcenter*/TestContext.DataRow["workcenter"].ToString();
            string group = /*Automation.Properties.TestData.Default.group*/TestContext.DataRow["group"].ToString();
            string url = Automation.Properties.TestData.Default.Url;
            string MO = /*Automation.Properties.TestData.Default.mo*/TestContext.DataRow["mo"].ToString();
            string qty = /*"1000"*/TestContext.DataRow["qty"].ToString();
            //string scrapqty = /*Automation.Properties.TestData.Default.scrapqty*/TestContext.DataRow["scrapqty"].ToString();
            string scrapcode = "OS  ";
            string RFID = /*Automation.Properties.TestData.Default.RFID*/TestContext.DataRow["rfid"].ToString();
            string palletcomplete = /*Automation.Properties.TestData.Default.palletcomplete*/TestContext.DataRow["palletcomplete"].ToString();
            //string reason = Automation.Properties.TestData.Default.DTreason;
            //string palletcomplete1 = Automation.Properties.TestData.Default.palletComplete1;
            //string scrapcode1 = Automation.Properties.TestData.Default.scrapCode1;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();

            // 2, Observe the Priority List Screen
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // Pre-Requisite
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // 3, Observe the lower grid
            elm_row = PriorityListPage.row_zeroBox1.Init().GetChildren();
            Assert.IsTrue(elm_row[1].GetAttribute("class").Contains("green"), "MO not in green color after 30");
            WebArchive.WaitForPageLoad();

            // 4, Select any order from the upper grid
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 5, Select Wand On Labor 30 button
            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 6, Verify the 35 screen
            // 7, Enter Qty which exceeds the Qty required but within the allow percentage range
            // 8, Select pallet complete and click ok
            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);

            // 9, Verify the soft error alert
            Assert.IsFalse(status.Contains("Error"), status);
            PriorityListPage.btn_overlayCancel.Init().ClickControl();
            CommonFunctions.Wait(browser);

            // 10, Select Cancel buton

            // 11, Select Ok in the Alert pop up
            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);

            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }
            #endregion

            #region Test Cleanup

            #endregion

        }

        [TestMethod]
        [WorkItem(114934)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(2)]
        [Description("Verify that soft error throws when a MO is completed by someoneelse")]
        public void VerifyTheSoftErrorThrowsWhenAMOIsCompletedBySomeoneelse()
        {
            driversByHandles = new Dictionary<string, IWebDriver>();
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;
            IWebElement elm_btn_35WandOff = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string username1 = "006009";
            string password1 = "006009";
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string reason = Automation.Properties.TestData.Default.DTreason;
            string palletcomplete1 = Automation.Properties.TestData.Default.palletComplete1;
            string scrapcode1 = Automation.Properties.TestData.Default.scrapCode1;

            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            IWebDriver dryuiver = GlobalProperties.driver;

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();

            // 2, Observe the Priority List Screen
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 3, Select an order from Upper Grid
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 4, Select Wand On 30
            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 5, Update RFID Tag
            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // 6, Login to Operator with another credentials and select same configuration
            IWebDriver driver = new ChromeDriver(@"C:\ToWork\Ashley\QA\DirectedWork\Ashley.Manufacturing.DirectedWork.Automation\TestDriver");
            driver.Navigate().GoToUrl(url);
            driver.Manage().Window.Maximize();
            IWebElement elm_txt_userName = driver.FindElement(By.Id("UserName"));
            elm_txt_userName.SendKeys(username1);

            IWebElement elm_txt_password = driver.FindElement(By.Id("Password"));
            elm_txt_password.SendKeys(password1);
            IWebElement elm_btn_login = driver.FindElement(By.ClassName("loginBtn"));
            elm_btn_login.Click();
            Thread.Sleep(2000);

            IWebElement elm_div_container = driver.FindElement(By.Id("slider1_container"));
            List<IWebElement> list = elm_div_container.FindElements(By.TagName("input")).ToList();
            IWebElement elm_operatorSite = list.First(x => x.GetAttribute("value").ProcessString() == site.ProcessString());
            elm_operatorSite.Click();
            Thread.Sleep(3000);

            IWebElement elm_ul_containerZone = driver.FindElement(By.ClassName("zonebtn"));
            list = elm_ul_containerZone.FindElements(By.TagName("input")).ToList();
            IWebElement elm_zone = list.First(x => x.GetAttribute("value").ProcessString() == zone.ProcessString());
            elm_zone.Click();
            Thread.Sleep(3000);

            IWebElement elm_div_containerWorkCenter = driver.FindElement(By.Id("slider1_container"));
            list = elm_div_containerWorkCenter.FindElements(By.TagName("input")).ToList();
            IWebElement elm_workCenter = list.First(x => x.GetAttribute("value").ProcessString() == workCenter.ProcessString());
            elm_workCenter.Click();
            Thread.Sleep(3000);

            IWebElement elm_div_containerOperatorGroup = driver.FindElement(By.Id("responsive"));
            IWebElement elm_operatorGroup = elm_div_containerOperatorGroup.FindElement(By.TagName("select"));
            var selectElement = new SelectElement(elm_operatorGroup);
            selectElement.SelectByText(group);
            Thread.Sleep(2000);

            IWebElement elm_btn_OK = driver.FindElement(By.Id("saveEdit"));
            elm_btn_OK.Click();
            Thread.Sleep(5000);

            // 7, select the same order which is already selected by another user
            IWebElement elm_searchMo = driver.FindElement(By.Id("txtMOFilter"));
            elm_searchMo.SendKeys(MO);
            IWebElement elm_btn_MoOK = driver.FindElement(By.Id("basic-addon2"));
            elm_btn_MoOK.Click();
            Thread.Sleep(2000);

            IWebElement elm_zeroRow = driver.FindElement(By.Id("row0box"));
            elm_zeroRow.Click();
            Thread.Sleep(2000);

            // 8, Select Wand On 30
            IWebElement elm_btn_30wand = driver.FindElement(By.Id("btnWndOnLbr"));
            elm_btn_30wand.Click();
            Thread.Sleep(2000);

            // 9,Select pallet complete and click ok
            IWebElement elm_palletComplete = driver.FindElement(By.Id("ddlPLComplete0"));

            var selectElement1 = new SelectElement(elm_palletComplete);
            selectElement1.SelectByValue(palletcomplete);

            IWebElement elm_btn_35okBtn = driver.FindElement(By.Id("btnOk"));
            elm_btn_35okBtn.Click();
            Thread.Sleep(2000);

            List<IWebElement> elm_btn_overlayOkBtn = driver.FindElements(By.ClassName("ui-button-text")).ToList();
            elm_btn_overlayOkBtn[1].Click();
            Thread.Sleep(2000);
            driver.Quit();


            // 10, Login as operator with previous login credentials           
            // 11, Select Wand Off 35 button

            elm_btn_35WandOff = PriorityListPage.btn_35WandOff.Init();
            elm_btn_35WandOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 12, Select pallet complete no and click ok button
            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsTrue(GlobalProperties.driver.SwitchTo().Alert().Text.Contains("The selected MO Order will be removed from the list"));
            GlobalProperties.driver.SwitchTo().Alert().Accept();
            #endregion


            #region Test Cleanup


            #endregion
        }

        [TestMethod]
        [WorkItem(114953)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(2)]
        [Description("Verify that soft error throws when Completed Pieces/ Scrap Pieces/Rework Qty is more than the allowed percentage")]
        public void VerifyThatSoftErrorThrowsWhenCompletedPieces_ScrapPieces_Rework_QtyIsMoreThanTheAllowedPercentage()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;
            IWebElement elm_35WandOff = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = "500";
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string reason = Automation.Properties.TestData.Default.DTreason;
            string palletcomplete1 = Automation.Properties.TestData.Default.palletComplete1;
            string scrapcode1 = Automation.Properties.TestData.Default.scrapCode1;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();

            // 2, Observe the Priority List Screen
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // Pre-Requisite
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // 5, Select Wand Off 35
            elm_35WandOff = PriorityListPage.btn_35WandOff.Init();
            elm_35WandOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 6, Enter Qty which is beyond the required level
            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);

            // 7, Select ok
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            #endregion

            #region Test Cleanup

            #endregion
        }

        [TestMethod]
        [WorkItem(114955)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(3)]
        [Description("Verify the soft error when scrap code is invalid")]
        public void VerifyTheSoftErrorWhenScrapCodeIsInvalid()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;
            IWebElement elm_35WandOff = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string reason = Automation.Properties.TestData.Default.DTreason;
            string palletcomplete1 = Automation.Properties.TestData.Default.palletComplete1;
            string scrapcode1 = "RW  ";


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();

            // 2, Observe the Priority List Screen
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // Pre-Requisite
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 4 , Select Wand On 30
            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // 5, Select Wand Off 35
            elm_35WandOff = PriorityListPage.btn_35WandOff.Init();
            elm_35WandOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 6, Enter Qty as minimal Qty
            // 7, Select the pallet complete value from dropdown
            // 8, Select invalid scrap Code and Ok buton
            status = PriorityListPage.WandOff35(qty, scrapcode1, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);

            // 7, Select ok
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {

                var sf = PriorityListPage.overlay_Information.Init().Text;
                // Assert.IsTrue(PriorityListPage.overlay_Information.Init().Text.Contains("INVALID SCRAP CODE"));
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            #endregion

            #region Test Cleanup

            #endregion

        }

        [TestMethod]
        [WorkItem(118562)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(2)]
        [Description("Verify the operator 'Logout' functionality when DCS transaction is ON")]
        public void VerifyTheOperatorLogoutFunctionalityWhenDCSTransactionIsON()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;
            IWebElement elm_35WandOff = null;
            IWebElement elm_btn_20SetUpTimeOn = null;
            IWebElement elm_btn_25SetUpTimeOff = null;
            IWebElement elm_btn_50DownTimwOn = null;
            IWebElement elm_55DTOff = null;
            IWebElement elm_btn_55DownTimeOff = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string reason = Automation.Properties.TestData.Default.DTreason;
            string palletcomplete1 = Automation.Properties.TestData.Default.palletComplete1;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();

            // 1.9, Check all the button mode
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 1.10, Search an @Order(MO) from upper grid
            // 1.11, Click over the Order searched
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 1.12, Click on 'Wand On Labor (30)' button
            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 1.13, Enter @RFID and click on button 'OK'
            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // 1.14, Check 'Group#' is blinking
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");

            // 2, Select Logout button
            CommonFunctions.LogOut();

            // 3, Select OK in the alert pop up
            var alertText = GlobalProperties.driver.SwitchTo().Alert().Text;
            GlobalProperties.driver.SwitchTo().Alert().Accept();
            Assert.AreEqual("Please Wand Off (35) the selected order", alertText);

            // 4, Select 35 Wand Off button
            elm_35WandOff = PriorityListPage.btn_35WandOff.Init();
            elm_35WandOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 5, Select Pallet Complete 'Yes' or 'No'
            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);
            WebArchive.WaitForPageLoad();
            // 6, Select ok
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            CommonFunctions.LogOut();

            // 7, Machine Operator User - Successful 20 Setup Time On
            // 7.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            // 7.2, Enter valid @Operatorusername in Username field
            // 7.3, Enter valid @Operatorpassword in Password field
            // 7.4, Click on "Login" button           
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 7.5, Select @OperatorSite
            // 7.6, Select @OperatorZone
            // 7.7, Select @OperatorWorkCenter
            // 7.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();

            // 7.9, Search @Order(MO) from upper grid
            // 7.10, Click over the order searched
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 7.11, Click on '20 Setup Time On' button
            elm_btn_20SetUpTimeOn = PriorityListPage.btn_20SetupTimeOn.Init();
            elm_btn_20SetUpTimeOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 1.12, Button Status are
            // Enabled button status           
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 1.13, Check 'Group#' is blinking
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");

            // 2, Select Logout button
            CommonFunctions.LogOut();

            // 9, Select OK in the alert pop up
            alertText = GlobalProperties.driver.SwitchTo().Alert().Text;
            GlobalProperties.driver.SwitchTo().Alert().Accept();
            Assert.AreEqual("Please Setup Off (25) the selected order", alertText);

            // 10, Select 25 Set Up Off button
            elm_btn_25SetUpTimeOff = PriorityListPage.btn_25SetupTimeOff.Init();
            elm_btn_25SetUpTimeOff.ClickControl();

            // 11, Select Ok
            status = PriorityListPage.SetupTimeOff25(qty, scrapqty, scrapcode);
            Assert.IsFalse(status.Contains("Error"), status);
            WebArchive.WaitForPageLoad();

            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }

            CommonFunctions.LogOut();

            // 12, Machine Operator User - Successful 50 Indirect/DT On
            // 12.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            // 12.2, Enter valid @Operatorusername in Username field
            // 12.3, Enter valid @Operatorpassword in Password field
            // 12.4, Click on "Login" button
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 12.5, Select @OperatorSite
            // 12.6, Select @OperatorZone
            // 12.7, Select @OperatorWorkCenter
            // 12.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();

            // 12.9, Click on '50 Indirect/DT On' button
            elm_btn_50DownTimwOn = PriorityListPage.btn_50IndirectDtOn.Init();
            elm_btn_50DownTimwOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 12.10, Select @DownTimeCode
            // 12.11, Click on button 'OK' button with/without comments
            PriorityListPage.DownTimeOnReason(reason);

            // 12.12, Button Status are
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 12.13, Check 'Group#' is blinking
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");

            // 13, Select Logout button
            CommonFunctions.LogOut();

            // 14, Select OK in the alert pop up
            alertText = GlobalProperties.driver.SwitchTo().Alert().Text;
            GlobalProperties.driver.SwitchTo().Alert().Accept();
            Assert.AreEqual("Please DownTime Off (55) the selected order", alertText);

            // 15, Select 55 Indirect DT Off button
            elm_55DTOff = PriorityListPage.btn_55IndirectDtOff.Init();
            elm_55DTOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 16, Select Ok
            elm_btn_55DownTimeOff = PriorityListPage.btn_downTimeOff.Init();
            elm_btn_55DownTimeOff.ClickControl();
            WebArchive.WaitForPageLoad();
            CommonFunctions.Wait(browser);

            // 17, Select Logout Button
            CommonFunctions.LogOut();
            #endregion

            #region Test Cleanup

            #endregion

        }

        [TestMethod]
        [WorkItem(124825)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(2)]
        [Description("Verify the orders in lower grid are retained after re-opening the closed browser")]
        public void VerifyTheOrdersInLowerGridAreRetainedAfterRe_openingTheClosedBrowser()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_30WandOn = null;
            IWebElement elm_popUp_RFID = null;
            IWebElement elm_btn_RfidOk = null;
            IWebElement elm_35WandOff = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string reason = Automation.Properties.TestData.Default.DTreason;
            string palletcomplete1 = Automation.Properties.TestData.Default.palletComplete1;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();

            // 2,Select an MO from the Upper Grid
            status = PriorityListPage.EnterMO(MO);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);

            // 3, Click on 'Wand On Labor (30)' button
            elm_btn_30WandOn = PriorityListPage.btn_wandOnLabour30.Init();
            elm_btn_30WandOn.ClickControl();
            CommonFunctions.Wait(browser);

            // 4, Enter @RFID and click on button 'OK'
            elm_popUp_RFID = PriorityListPage.txt_RFID.Init();
            elm_popUp_RFID.EnterText(RFID);

            elm_btn_RfidOk = PriorityListPage.btn_OK.Init();
            elm_btn_RfidOk.ClickControl();
            CommonFunctions.Wait(browser);

            // 5, Close the Browser
            GlobalProperties.driver.Close();

            // 6, Launch the browser and Navigate to the same page
            CommonFunctions.LaunchBrowser(url);

            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();

            // 7, Verify the lower gird
            Assert.IsTrue(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");

            // 8, Select 35 Wand off button
            elm_35WandOff = PriorityListPage.btn_35WandOff.Init();
            elm_35WandOff.ClickControl();
            CommonFunctions.Wait(browser);

            // 9, Select Pallet Complete 'Yes' or 'No'
            status = PriorityListPage.WandOff35(qty, scrapcode, palletcomplete);
            Assert.IsFalse(status.Contains("Error"), status);
            WebArchive.WaitForPageLoad();
            // 10, Select ok
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
                CommonFunctions.Wait(browser);
            }
            #endregion

            #region Test Cleanup

            #endregion
        }

        [TestMethod]
        [WorkItem(118564)]
        [TestCategory("Regression - DCS Transcation")]
        [Priority(2)]
        [Description("Perform 60 and 65 transaction when he is out of group terminal")]
        public void Perform60And65TransactionWhenHeIsOutOfGroupTerminal()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_60AddToGroup = null;
            IWebElement elm_btn_65FromGroup = null;
            IWebElement elm_btn_infoCancel = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string MO = Automation.Properties.TestData.Default.mo;
            string qty = Automation.Properties.TestData.Default.qty;
            string scrapqty = Automation.Properties.TestData.Default.scrapqty;
            string scrapcode = Automation.Properties.TestData.Default.scrapcode;
            string RFID = Automation.Properties.TestData.Default.RFID;
            string palletcomplete = Automation.Properties.TestData.Default.palletcomplete;
            string reason = Automation.Properties.TestData.Default.DTreason;
            string palletcomplete1 = Automation.Properties.TestData.Default.palletComplete1;
            string EmpNo = "101";


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();

            // 1.9, Check all the button mode
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 1.10, Select '60 Add To Group' button
            elm_btn_60AddToGroup = PriorityListPage.btn_60AddToGroup.Init();
            elm_btn_60AddToGroup.ClickControl();

            // 1.11, Enter @Employee number who is removed from other group
            // 1.12, Select Ok
            // 1.13, Select ok in the pop up
            string result = PriorityListPage.AddEmployeeToGroup(EmpNo);

            // 2, Check the status of 'Group#'
            Assert.IsFalse(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");

            // 3, Check all the button mode
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            // 4, Select 65 button
            elm_btn_65FromGroup = PriorityListPage.btn_65RemoveFromGroup.Init();
            elm_btn_65FromGroup.ClickControl();

            // 5, Verify the Pop up
            // 6, Enter @Employee number
            // 7, select ok
            // 9, Select ok in the Confirmation pop up
            result = PriorityListPage.RemoveEmployeeFromGroup(EmpNo);

            // 8, Verify the Confirmation pop up
            Assert.IsTrue(result.Contains("has been removed from"));

            // 10, Select ok in the pop up
            elm_btn_infoCancel = PriorityListPage.btn_removeFromGroup.Init();
            elm_btn_infoCancel.ClickControl();

            // 11, Check the status of 'Group#'
            Assert.IsFalse(PriorityListPage.spn_GroupId.Init().GetAttribute("class") == "blink_Group", "Group Id is not blinking after generating the order");

            // 12, Check the status of buttons
            Assert.IsTrue(PriorityListPage.btn_reqWork.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_60AddToGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_65RemoveFromGroup.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_50IndirectDtOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsTrue(PriorityListPage.btn_addRowForManualWandOn.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");

            // Diabled button status           
            Assert.IsFalse(PriorityListPage.btn_wandOnLabour30.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_20SetupTimeOn.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_55IndirectDtOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_35WandOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");
            Assert.IsFalse(PriorityListPage.btn_25SetupTimeOff.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            #endregion

            #region Test Cleanup

            #endregion
        }



        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
            TestContext.TestInitProcess();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.TestDisposal();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
