﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QA.AutomationLibrary;
using Ashley.Manufacturing.DirectedWork.Automation.Pages;
using OpenQA.Selenium;

namespace Ashley.Manufacturing.DirectedWork.Automation.Tests
{
    [TestClass]
    [DeploymentItem(@"TestDriver", "TestDriver")]
    public class Regression_DCSTransaction
    {       

        [TestInitialize]
        public void TestInitialize()
        {

        }

 

        //   [TestMethod]
        //   [WorkItem(118153)]
        //   [Description("Check users are allowed to do '25 Setup Time Off' DCS transaction after 20 Setup Time On")]
        ////   [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.Manufacturing", "118153", DataAccessMethod.Sequential)]
        //   public void CheckUsersAreAllowedToDo25SetupTimeOffDCSTranscationAfter20SetupTimeOn()
        //   {
        //       #region Local Variables

        //       Object timeOfClicked = null;
        //       Dictionary<string, Object> dic_datasfromDB = new Dictionary<string, object>();
        //       DateTime dateTimeFromDB;
        //       string timeFromDB = string.Empty;
        //       string timeClickedByApp = string.Empty;

        //       #endregion

        //       #region TestData

        //       //string operatorUrl = TestContext.DataRow["OperatorURL"].ToString();
        //       //string operatorUserName = TestContext.DataRow["Operatorusername"].ToString();
        //       //string operatorPassword = TestContext.DataRow["Operatorpassword"].ToString();
        //       //string operatorSite = TestContext.DataRow["OperatorSite"].ToString();
        //       //string operatorZone = TestContext.DataRow["OperatorZone"].ToString();
        //       //string operatorWorkCenter = TestContext.DataRow["OperatorWorkCenter"].ToString();
        //       //string group = TestContext.DataRow["Group"].ToString();
        //       //string order = TestContext.DataRow["Order"].ToString();
        //       //string RFID = TestContext.DataRow["RFID"].ToString();
        //       string browserName = "chrome";
        //       string operatorUrl = "http://stagemfg.ashleyfurniture.com/ashley.manufacturing.directedwork.operators/ashley/login.html";
        //       string operatorUserName = "000101";
        //       string operatorPassword = "000101";
        //       string operatorSite = "Arcaadia";
        //       string operatorZone = "PLT3";
        //       string operatorWorkCenter = "WE104";
        //       string group = "926";
        //       string order = "MU53030";



        //       #endregion

        //       #region Test Steps

        //       //#region Launch webdriver

        //       //Launch selenum webdriver
        //       driver = Selenium.LaunchDriver(false, null, browserName);

        //       driver.Manage().Window.Maximize();

        //       driver.Navigate().GoToUrl(operatorUrl);

        //       #endregion

        //       #region Machine Operator User - Successful 20 Setup Time On
        //       //1. Launch "Machine Operator" @OperatorURL and click on "Enter" button
        //       //2. Enter valid @Operatorusername in Username field
        //       //3. Enter valid @Operatorpassword in Password field
        //       //4. Click on "Login" button
        //       driver.Login(operatorUserName, operatorPassword);

        //       //5. Select @OperatorSite
        //       driver.ChooseOperatorSite(operatorSite);

        //       //6. Select @OperatorZone
        //       driver.ChooseZone(operatorZone);

        //       //7. Select @OperatorWorkCenter
        //       driver.ChooseWorkCenter(operatorWorkCenter);

        //       //8. Select @Group and click on 'OK' button
        //       driver.ChooseOperatorGroup(group);

        //       //9. Search an @Order(MO) from upper grid 
        //       //10. Click over the order searched           
        //       driver.EnterMO(order);           

        //       //11. Click on '20 Setup Time On' button
        //       timeOfClicked = driver.SetUpTimeOn20();

        //       //12. Button Status are
        //       //13.Check 'Group#' is blinking
        //       driver.VerifyAssertionAfter20();
        //       #endregion

        //       #region DB Validation

        //       dic_datasfromDB = iSeriesDBCommonFunctions.GetDatasFromiSeriesafter20(order, group);

        //       dateTimeFromDB = ((DateTime)(dic_datasfromDB["DCTTrans_DATE"]));

        //       timeFromDB = dic_datasfromDB["DCTTRANS_TIME"].ToString();

        //       if (timeFromDB.Length >= 5)
        //       {
        //           timeClickedByApp = ((DateTime)(timeOfClicked)).ToString("hmmss");
        //       }
        //       else if (timeFromDB.Length == 4 || timeFromDB.Length == 3)
        //       {
        //           timeClickedByApp = ((DateTime)(timeOfClicked)).ToString("mss");
        //       }

        //       //Assert Verification between the values of DB and the application
        //       Assert.AreEqual(((DateTime)(timeOfClicked)).ToString("dd-MM-yyyy"), dateTimeFromDB.ToString("dd-MM-yyyy"), " ");

        //       if (timeClickedByApp == timeFromDB)
        //           Assert.AreEqual(timeClickedByApp, timeFromDB, " ");
        //       else
        //           Assert.AreEqual(int.Parse(timeClickedByApp) + 1, int.Parse(timeFromDB), " ");

        //       Assert.AreEqual(group, dic_datasfromDB["DCTGROUPNUMBER"].ToString(), "Given group " + group + " does not match with the DataBase Order : " + dic_datasfromDB["DCTGROUPNUMBER"].ToString());

        //       Assert.AreEqual(order, dic_datasfromDB["DCTORDER#"].ToString(), "Given order " + order + " does not match with the DataBase Order : " + dic_datasfromDB["DCTORDER#"].ToString());

        //       #endregion


        //   }
        [TestCleanup]
        public void TestCleanup()
        {
            //driver.Quit();
        }

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
