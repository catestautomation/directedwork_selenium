﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QA.AutomationLibrary;
using Ashley.Manufacturing.DirectedWork.Automation.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;

namespace Ashley.Manufacturing.DirectedWork.Automation.Tests
{
    [TestClass]
    [DeploymentItem(@"TestDriver", @"TestDriver")]
    public class Reg_VeronaBedding_DCSTranscation
    {
        string browser = Automation.Properties.TestData.Default.browser;

        [TestMethod]
        [WorkItem(116892)]
        [TestCategory("Regression-Verona")]
        [Priority(3)]
        [Description("Check that Cancel button should cancel the scanning process")]
        public void CheckThatCancelButtonShouldCancelTheScanningProcess()
        {

            #region Local Variables
            IWebElement elm_lbl_siteHeader = null;
            string status = string.Empty;
            IWebElement elm_btn_beginScan = null;
            IWebElement elm_lbl_serialScanning = null;
            IWebElement elm_btn_serialScanCancel = null;
            IWebElement elm_lbl_priorityListHeader = null;

            #endregion

            #region Test Data
            string url = Automation.Properties.TestData.Default.Url;
            string username = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.Veronasite;
            string zone = Automation.Properties.TestData.Default.Veronazone;
            string workcenter = Automation.Properties.TestData.Default.Veronaworkcenter;
            string group = Automation.Properties.TestData.Default.Veronagroup;
            #endregion

            #region Test Steps

            //1. Launch "Operators" URL
            CommonFunctions.LaunchBrowser(url);

            //2. Login to the application
            CommonFunctions.Login(username, password);
            elm_lbl_siteHeader = OperatorSitesPage.lbl_siteHeader.Init();
            Assert.AreEqual(true, elm_lbl_siteHeader.Displayed);

            //3. Select the Site
            //4. Select the Zone       
            //5. Select the Workcentre          
            //6. Select the Group
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group);
            Assert.IsFalse(status.Contains("Error"), status);

            //7. Select Begin Scan button
            elm_btn_beginScan = PriorityListPage.btn_reqWork.Init();
            elm_btn_beginScan.ClickControl();

            elm_lbl_serialScanning = PriorityListPage.lbl_serialScanning.Init();
            Assert.AreEqual(true, elm_btn_beginScan.Displayed);

            //8. Click Cancel button

            elm_btn_serialScanCancel = PriorityListPage.btn_serialScanCancel.Init();
            elm_btn_serialScanCancel.ClickControl();

            elm_lbl_priorityListHeader = PriorityListPage.lbl_priorityListHeader.Init();
            Assert.AreEqual(true, elm_lbl_priorityListHeader.Displayed);

            #endregion

            #region Test Cleanup

            #endregion

        }

        [TestMethod]
        [WorkItem(116891)]
        [TestCategory("Regression-Verona")]
        [Priority(1)]
        [Description("Check that it allows to perform 50 followed by a 50 again")]
        public void CheckThatItAllowsToPerform50FollowedByA50Again()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_50IndirectDtOn = null;
            IWebElement elm_btn_55IndirectDtOff = null;
            IWebElement elm_btn_55IndirectDtOffOK = null;
            #endregion

            #region Test Data
            string url = Automation.Properties.TestData.Default.Url;
            string username = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.Veronasite;
            string zone = Automation.Properties.TestData.Default.Veronazone;
            string workcenter = Automation.Properties.TestData.Default.Veronaworkcenter;
            string group = Automation.Properties.TestData.Default.Veronagroup;
            string donwTimeCode = Automation.Properties.TestData.Default.DTreason;
            #endregion

            #region Test Steps

            //1. Launch "Operators" URL
            CommonFunctions.LaunchBrowser(url);

            //2. Login to the application
            CommonFunctions.Login(username, password);
            IWebElement elm_lbl_siteHeader = OperatorSitesPage.lbl_siteHeader.Init();
            Assert.AreEqual(true, elm_lbl_siteHeader.Displayed);

            //3. Select the Site
            //4. Select the Zone       
            //5. Select the Workcentre          
            //6. Select the Group
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workcenter, group);
            Assert.IsFalse(status.Contains("Error"), status);

            //7. Select 50 IndirectDownTime Button and Choose the Code
            elm_btn_50IndirectDtOn = PriorityListPage.btn_50IndirectDtOn.Init();
            elm_btn_50IndirectDtOn.ClickControl();

            PriorityListPage.DownTimeOnReason(donwTimeCode);

            //8. Click on 55 IndirectDownTime Button

             elm_btn_55IndirectDtOff = PriorityListPage.btn_55IndirectDtOff.Init();
            elm_btn_55IndirectDtOff.ClickControl();       

            elm_btn_55IndirectDtOffOK = PriorityListPage.btn_downTimeOff.Init();
            elm_btn_55IndirectDtOffOK.ClickControl();

            //9. Again Click on 50 IndirectDownTime Button
            elm_btn_50IndirectDtOn = PriorityListPage.btn_50IndirectDtOn.Init();
            elm_btn_50IndirectDtOn.ClickControl();

            PriorityListPage.DownTimeOnReason(donwTimeCode);

            // 10. Complete the 55 Action
            elm_btn_55IndirectDtOff = PriorityListPage.btn_55IndirectDtOff.Init();
            elm_btn_55IndirectDtOff.ClickControl();

            elm_btn_55IndirectDtOffOK = PriorityListPage.btn_downTimeOff.Init();
            elm_btn_55IndirectDtOffOK.ClickControl();
            #endregion

            #region Test Cleanup

            #endregion



        }

        [TestMethod]
        [WorkItem(116886)]
        [TestCategory("Regression - Verone Wedding")]
        [Priority(2)]
        [Description("Check that Transaction is retained when system shuts down/power off/network outrage")]
        public void CheckThatTransactionIsRetainedWhenSystemShutsDownOrPowerOffOrNetworkOutrage()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_BeginScanning = null;
            
            #endregion

            #region Test Data
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.Veronasite;
            string zone = Automation.Properties.TestData.Default.Veronazone;
            string workCenter = Automation.Properties.TestData.Default.Veronaworkcenter;
            string group = Automation.Properties.TestData.Default.Veronagroup;
            string url = Automation.Properties.TestData.Default.Url;
            string serial1 ="" /*Automation.Properties.TestData.Default.Veroneserial1*/;
            List<IWebElement> elm_row2 = null;

            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button

            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);


            // 2,3,4, Select @Zone, @Site, @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);

            // 5, Click on Begin Scanning button
            elm_btn_BeginScanning = PriorityListPage.btn_reqWork.Init();
            elm_btn_BeginScanning.ClickControl();

            // 6, Scan Serial number
            PriorityListPage.EnterVeroneSerial(serial1);
            WebArchive.WaitForPageLoad(7000, true);

            // Select  Cancel Button on Begin Scan Popup
            // elm_btn_cancel = PriorityListPage.btn_cancel.Init();
            // elm_btn_cancel.ClickControl();

            // 7, Introduce an interrupt by shutting down system or unplug network cable
            GlobalProperties.driver.Close();

            // 8, Login to the same group
            CommonFunctions.LaunchBrowser(url);
            status = CommonFunctions.Login(userName, password);

            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);

            // Verify Serial Number that we passed in step 6

            elm_row2 = PriorityListPage.tbl_orderSelectedContent.Init().GetAllVisibileChildren();
            Assert.IsTrue(elm_row2[0].Text.Contains(serial1), "Serial number is not same");

            CommonFunctions.Wait(browser);
            #endregion

            #region Test Cleanup

            #endregion
        }        

        [TestMethod]
        [WorkItem(116885)]
        [TestCategory("Regression - Verone Wedding")]
        [Priority(1)]
        [Description("Check multiple wand on for same order is allowed")]
        public void CheckMultipleWandOnForSameOrderIsAllowed()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_BeginScanning = null;
            IWebElement elm_btn_35WandOff = null;
            IWebElement elm_btn_okBtn = null;
            IWebElement elm_btn_cancel = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.Veronasite;
            string zone = Automation.Properties.TestData.Default.Veronazone;
            string workCenter = Automation.Properties.TestData.Default.Veronaworkcenter;
            string group = Automation.Properties.TestData.Default.Veronagroup;
            string url = Automation.Properties.TestData.Default.Url;
            string serial1 =""/*= Automation.Properties.TestData.Default.Veroneserial1*/;
            string serial2 ="" /*= Automation.Properties.TestData.Default.Veroneserial2*/;

            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button

            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            // 2, Click on Begin Scanning
            elm_btn_BeginScanning = PriorityListPage.btn_reqWork.Init();
            elm_btn_BeginScanning.ClickControl();

            // 3, Scan an Serial number from an order
            PriorityListPage.EnterVeroneSerial(serial1);
            WebArchive.WaitForPageLoad(7000, true);

            //scan another serial number form same order
            PriorityListPage.EnterVeroneSerial(serial2);
            WebArchive.WaitForPageLoad(7000, true);
            elm_btn_cancel = PriorityListPage.btn_cancel.Init();
            elm_btn_cancel.ClickControl();

            // 5, Select 35 button
            elm_btn_35WandOff = PriorityListPage.btn_35WandOff.Init();
            elm_btn_35WandOff.ClickControl();

            // Verify Quantity
            Assert.IsTrue(PriorityListPage.txt_wandOffQuantity.Init().GetAttribute("value").Equals("2"), "Quantity value is not correct");
            elm_btn_okBtn = PriorityListPage.btn_okBtn.Init();
            elm_btn_okBtn.ClickControl();
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
            }

            CommonFunctions.Wait(browser);
            #endregion

            #region Test Cleanup

            #endregion

        }

        [TestMethod]
        [WorkItem(117246)]
        [TestCategory("Regression - Verone Wedding")]
        [Priority(1)]
        [Description("Check multiple wand on for different order  is allowed")]
        public void CheckMultipleWandOnForDifferentOrderIsAllowed()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_BeginScanning = null;
            IWebElement elm_btn_35WandOff = null;
            IWebElement elm_btn_okBtn = null;
            IWebElement elm_btn_cancel = null;

            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.Veronasite;
            string zone = Automation.Properties.TestData.Default.Veronazone;
            string workCenter = Automation.Properties.TestData.Default.Veronaworkcenter;
            string group = Automation.Properties.TestData.Default.Veronagroup;
            string url = Automation.Properties.TestData.Default.Url;

            string serial1 = "" /*Automation.Properties.TestData.Default.Veroneserial1*/;
            string serial2 = ""/*Automation.Properties.TestData.Default.Veroneserial2*/;

            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button

            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            // 2, Click on Begin Scanning
            elm_btn_BeginScanning = PriorityListPage.btn_reqWork.Init();
            elm_btn_BeginScanning.ClickControl();

            // 3, Scan an Serial number from an order
            PriorityListPage.EnterVeroneSerial(serial1);
            WebArchive.WaitForPageLoad(7000, true);

            //scan another serial number form different order
            PriorityListPage.EnterVeroneSerial(serial2);
            WebArchive.WaitForPageLoad(7000, true);
            elm_btn_cancel = PriorityListPage.btn_cancel.Init();
            elm_btn_cancel.ClickControl();



            // 5, Select 35 button
            elm_btn_35WandOff = PriorityListPage.btn_35WandOff.Init();
            elm_btn_35WandOff.ClickControl();

            //Verify that we have two rwos for different order serial number
            Assert.IsTrue(PriorityListPage.row_wandOff01.Init().GetAttribute("id").Equals("0"), "row 1 not Present");
            Assert.IsTrue(PriorityListPage.row_wandOff02.Init().GetAttribute("id").Equals("1"), "row 2 not Present");


            elm_btn_okBtn = PriorityListPage.btn_okBtn.Init();
            elm_btn_okBtn.ClickControl();
            if (PriorityListPage.overlay_Information.Init().GetCssValue("display") == "block")
            {
                PriorityListPage.btn_overlayOk.Init().ClickControl();
            }

            CommonFunctions.Wait(browser);
            #endregion

            #region Test Cleanup

            #endregion

        }       

        [TestMethod]
        [WorkItem(116534)]
        [TestCategory("Regression - Verone Wedding")]
        [Priority(1)]
        [Description("Check that 55 screen appears when scanning an Mo with 50 done")]
        public void CheckThat55ScreenAppearsWhenScanningAnMOwith50done()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_50IndirectDtOn = null;
            IWebElement elm_btn_55IndirectDtOff = null;
            IWebElement elm_drpdwn_downTime_Code = null;
            IWebElement elm_btn_OK_downTime_On = null;
            IWebElement elm_btn_serial_scan = null;
            IWebElement elm_txt_serial_scan = null;
            IWebElement elm_btn_serial_scan_OK = null;
            IWebElement elm_btn_serial_scan_cancel = null;
            //IWebElement elm_tcol_lowerGrid = null;

            #endregion

            #region Test Data
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workCenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            string url = Automation.Properties.TestData.Default.Url;
            string code = Automation.Properties.TestData.Default.DTreason;
            string serialNumber = ""; /*Automation.Properties.TestData.Default.serialNumber;*/

            #endregion

            #region Test Script
            //1, Machine Operator User - Steps to Login DirectedWork Site
            //1.1, Enter valid @Operatorusername in Username field          
            CommonFunctions.LaunchBrowser(url);

            //1.2, Launch "Machine Operator" @OperatorURL and click on "Enter" button
            //1.3, Enter valid @Operatorpassword in Password field
            //1.4, Click on "Login" button
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            //1.5, Select @OperatorSite
            //1.6, Select @OperatorZone
            //1.7, Select @OperatorWorkCenter
            //1.8, Select @Group and click on 'OK' button

            //2, Select @Zone
            //3, Select @workcentre
            //4, Select @Group
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);

            //5, Select 50
            elm_btn_50IndirectDtOn = PriorityListPage.btn_50IndirectDtOn.Init();
            elm_btn_50IndirectDtOn.ClickControl();

            elm_drpdwn_downTime_Code = PriorityListPage.drpdwn_downTime_Code.Init();
            var selectElement = new SelectElement(elm_drpdwn_downTime_Code);
            selectElement.SelectByIndex(7);

            elm_btn_OK_downTime_On = PriorityListPage.btn_downtimeOk.Init();
            elm_btn_OK_downTime_On.ClickControl();

            //6, Select Begin scan button
            elm_btn_serial_scan = PriorityListPage.btn_reqWork.Init();
            elm_btn_serial_scan.ClickControl();

            GlobalProperties.driver.SwitchTo().Alert().Accept();

            //7, select ok in the 55 screen
            elm_btn_55IndirectDtOff = PriorityListPage.btn_55IndirectDtOff.Init();
            elm_btn_55IndirectDtOff.ClickControl();

            //8, Verify the screen
            elm_txt_serial_scan = PriorityListPage.txt_serial.Init();
            Assert.IsTrue(elm_txt_serial_scan.IsVisible(), "Element is not Visible");

            //9, Scan a serial number
            elm_txt_serial_scan.EnterText(serialNumber);

            elm_btn_serial_scan_OK = PriorityListPage.btn_reqWork.Init();
            elm_btn_serial_scan_OK.ClickControl();

            elm_btn_serial_scan_cancel = PriorityListPage.btn_cancel.Init();
            elm_btn_serial_scan_cancel.ClickControl();

            //10, Verify the lower Grid

            List<IWebElement> elm_row = null;
            elm_row = PriorityListPage.tcol_lowerGrid.Init().GetChildren();

            Assert.IsTrue(elm_row[0].Text.Contains(serialNumber));

            #endregion


            #region Test Cleanup

            #endregion

        }


        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            GlobalProperties.driver.Quit();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

    }
}
