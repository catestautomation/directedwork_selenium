﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QA.AutomationLibrary;
using Ashley.Manufacturing.DirectedWork.Automation.Pages;
using OpenQA.Selenium;


namespace Ashley.Manufacturing.DirectedWork.Automation.Tests
{
    [TestClass]
    [DeploymentItem(@"TestDriver", @"TestDriver")]
    public class Reg_OperatorLogin
    {
        string browser = Automation.Properties.TestData.Default.browser;
        [TestMethod]
        [WorkItem(132398)]
        [TestCategory("Regression - Login")]
        [Priority(1)]
        [Description("Validate the Operators Login functionality")]
        public void ValidateTheOperatorsLoginFunctionality()
        {

            #region Local Variables
            IWebElement elm_txt_userName = null;
            IWebElement elm_txt_password = null;
            IWebElement elm_btn_login = null;
            #endregion

            #region Test Data

            string url = Automation.Properties.TestData.Default.Url;
            #endregion

            #region Test Steps

            //1. Launch "Operators" URL
            CommonFunctions.LaunchBrowser(url);

            //2. Check for the 'Username' & 'Password' fields

            elm_txt_userName = LoginPage.txt_userName.Init();
            Assert.AreEqual(true, elm_txt_userName.Displayed);

            elm_txt_password = LoginPage.txt_password.Init();
            Assert.AreEqual(true, elm_txt_password.Displayed);

            //3. Check for the "Login" button

            elm_btn_login = LoginPage.btn_login.Init();
            Assert.AreEqual(true, elm_btn_login.Displayed);

            #endregion

            #region Test Cleanup

            #endregion

        }

        [TestMethod]
        [WorkItem(120916)]
        [TestCategory("Regression - Login")]
        [Priority(2)]
        [Description("Check the browser tab based on selection (Site, Zone and Workcenter)")]
        public void CheckTheBrowserTabBasedOnSiteZoneWorkcenterSelection()
        {

            #region Local Variables
            IWebElement elm_lbl_siteHeader = null;
            IWebElement elm_lbl_zoneHeader = null;
            IWebElement elm_lbl_wCenterHeader = null;
            IWebElement elm_lbl_groupHeader = null;
            IWebElement elm_lbl_priorityListHeader = null;
            #endregion

            #region Test Data
            string url = Automation.Properties.TestData.Default.Url;
            string username = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.site;
            string zone = Automation.Properties.TestData.Default.zone;
            string workcenter = Automation.Properties.TestData.Default.workcenter;
            string group = Automation.Properties.TestData.Default.group;
            #endregion

            #region Test Steps

            //1. Launch "Operators" URL
            CommonFunctions.LaunchBrowser(url);
           
            //2. Login to the application
            CommonFunctions.Login(username, password);
            CommonFunctions.Wait(browser);
            elm_lbl_siteHeader = OperatorSitesPage.lbl_siteHeader.Init();
            Assert.AreEqual(true, elm_lbl_siteHeader.Displayed);

            //3. Select the Site
            OperatorSitesPage.ChooseOperatorSite(site);
            CommonFunctions.Wait(browser);
            elm_lbl_zoneHeader = OperatorZonePage.lbl_zoneHeader.Init();
            Assert.AreEqual(true, elm_lbl_zoneHeader.Displayed);

            //4. Select the Browser Back
            GlobalProperties.driver.Navigate().Back();
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();
            elm_lbl_zoneHeader = OperatorZonePage.lbl_zoneHeader.Init();
            Assert.AreEqual(true, elm_lbl_zoneHeader.Displayed);

            //5. Select the Zone
            OperatorZonePage.ChooseZone(zone);
            CommonFunctions.Wait(browser);
            elm_lbl_wCenterHeader = OperatorWCenterPage.lbl_wCenterHeader.Init();
            Assert.AreEqual(true, elm_lbl_wCenterHeader.Displayed);

            //6. Select the Browser Back
            GlobalProperties.driver.Navigate().Back();
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();
            elm_lbl_wCenterHeader = OperatorWCenterPage.lbl_wCenterHeader.Init();
            Assert.AreEqual(true, elm_lbl_wCenterHeader.Displayed);

            //7. Select the Workcentre

            OperatorWCenterPage.ChooseWorkCenter(workcenter);
            CommonFunctions.Wait(browser);
            elm_lbl_groupHeader = OperatorGroupPage.lbl_groupHeader.Init();
            Assert.AreEqual(true, elm_lbl_groupHeader.Displayed);

            //8. Select the Browser Back
            GlobalProperties.driver.Navigate().Back();
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();
            elm_lbl_groupHeader = OperatorGroupPage.lbl_groupHeader.Init();
            Assert.AreEqual(true, elm_lbl_groupHeader.Displayed);

            //9. Select the Group
            OperatorGroupPage.ChooseOperatorGroup(group);
            CommonFunctions.Wait(browser);
            elm_lbl_priorityListHeader = PriorityListPage.lbl_priorityListHeader.Init();
            Assert.AreEqual(true, elm_lbl_priorityListHeader.Displayed);

            //10. Select the Browser Back
            GlobalProperties.driver.Navigate().Back();
            CommonFunctions.Wait(browser);
            WebArchive.WaitForPageLoad();
            elm_lbl_priorityListHeader = PriorityListPage.lbl_priorityListHeader.Init();
            Assert.AreEqual(true, elm_lbl_priorityListHeader.Displayed);
            CommonFunctions.Wait(browser);

            #endregion

            #region Test Cleanup

            #endregion

        }

        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            GlobalProperties.driver.Quit();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

    }
}
