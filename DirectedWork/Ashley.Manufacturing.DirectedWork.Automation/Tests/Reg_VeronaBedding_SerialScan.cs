﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QA.AutomationLibrary;
using Ashley.Manufacturing.DirectedWork.Automation.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;

namespace Ashley.Manufacturing.DirectedWork.Automation.Tests
{
    [TestClass]
    [DeploymentItem(@"TestDriver", @"TestDriver")]
    public class Reg_VeronaBedding_SerialScan
    {
        string browser = Automation.Properties.TestData.Default.browser;
        [TestMethod]

        [WorkItem(116397)]
        [TestCategory("Regression - Verone Wedding")]
        [Priority(3)]
        [Description("To Verify the 'Begin scanning' button allows to scan the serial number")]
        public void ToVerifyTheBeginScanningButtonAllowsToScanTheSerialNumber()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_BeginScanning = null;
            IWebElement elm_btn_cancel = null;


            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.Veronasite;
            string zone = Automation.Properties.TestData.Default.Veronazone;
            string workCenter = Automation.Properties.TestData.Default.Veronaworkcenter;
            string group = Automation.Properties.TestData.Default.Veronagroup;
            string url = Automation.Properties.TestData.Default.Url;
            string serial1 = "" /*Automation.Properties.TestData.Default.Veroneserial1*/;


            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button

            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);


            // 2,3,4, Select @Zone, @Site, @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);

            // 5, Select Begin Scanning
            elm_btn_BeginScanning = PriorityListPage.btn_reqWork.Init();
            elm_btn_BeginScanning.ClickControl();

            // Verify Serial textBox, OK Button and Cancel Button
            Assert.IsTrue(PriorityListPage.txt_serial.Init().GetAttribute("id").Contains("txtSerial"), "Serial Text Box not present");
            Assert.IsTrue(PriorityListPage.btn_OK.Init().GetAttribute("class").Contains("primary"), "Button is Disabled");
            Assert.IsFalse(PriorityListPage.btn_cancel.Init().GetAttribute("class").Contains("primary"), "Button is Enabled");

            //6, Select  Cancel Button on Begin Scan Popup
            elm_btn_cancel = PriorityListPage.btn_cancel.Init();
            elm_btn_cancel.ClickControl();

            // 7, Click on Begin Scanning button
            elm_btn_BeginScanning = PriorityListPage.btn_reqWork.Init();
            elm_btn_BeginScanning.ClickControl();
            // Verify that Serial txtBox should be present
            Assert.IsTrue(PriorityListPage.txt_serial.Init().GetAttribute("id").Contains("txtSerial"), "Serial Text Box not present");

            // 8,9, Scan Serial number
            PriorityListPage.EnterVeroneSerial(serial1);
            WebArchive.WaitForPageLoad(7000, true);

            CommonFunctions.Wait(browser);
            #endregion

            #region Test Cleanup

            #endregion

        }

        [TestMethod]
        [WorkItem(116437)]
        [TestCategory("Regression - Verone Wedding")]
        [Priority(2)]
        [Description("Check that on scanning serial number from a inactive group throughs alert message")]
        public void CheckThatOnScanningSerialNumberFromAnInactiveGroupThroughsAlertMessage()
        {
            #region Local Variables
            string status = string.Empty;
            IWebElement elm_btn_BeginScanning = null;
            #endregion

            #region Test Data
            //    string UserName = TestContext.DataRow["username"].ToString();
            string userName = Automation.Properties.TestData.Default.username;
            string password = Automation.Properties.TestData.Default.password;
            string site = Automation.Properties.TestData.Default.Veronasite;
            string zone = Automation.Properties.TestData.Default.Veronazone;
            string workCenter = Automation.Properties.TestData.Default.Veronaworkcenter;
            string group = Automation.Properties.TestData.Default.Veronagroup;
            string url = Automation.Properties.TestData.Default.Url;

            #endregion

            #region Test Steps

            //1, Machine Operator User - Successful 20 Setup Time On
            // 1.1, Launch "Machine Operator" @OperatorURL and click on "Enter" button

            CommonFunctions.LaunchBrowser(url);

            // 1.2, Enter valid @Operatorusername in Username field
            // 1.3, Enter valid @Operatorpassword in Password field
            // 1.4, Click on "Login" button            
            status = CommonFunctions.Login(userName, password);
            CommonFunctions.Wait(browser);
            Assert.IsFalse(status.Contains("Error"), status);

            // 1.5, Select @OperatorSite
            // 1.6, Select @OperatorZone
            // 1.7, Select @OperatorWorkCenter
            // 1.8, Select @Group and click on 'OK' button
            status = CommonFunctions.operatorSiteZoneWorkCenterGroupSelection(site, zone, workCenter, group);
            Assert.IsFalse(status.Contains("Error"), status);
            // 2, Click on Begin Scanning
            elm_btn_BeginScanning = PriorityListPage.btn_reqWork.Init();
            WebArchive.WaitForPageLoad(50, true);
            elm_btn_BeginScanning.Click();


            // 3, Alert Handling         

            var alertText = GlobalProperties.driver.SwitchTo().Alert().Text;
            Assert.AreEqual("Group : 5863 is not active.", "Group : 5863 is not active.");
            GlobalProperties.driver.SwitchTo().Alert().Accept();

            CommonFunctions.Wait(browser);
            #endregion

            #region Test Cleanup

            #endregion

        }

        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            GlobalProperties.driver.Quit();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
