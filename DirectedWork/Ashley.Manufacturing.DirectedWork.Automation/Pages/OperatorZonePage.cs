﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QA.AutomationLibrary;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;

namespace Ashley.Manufacturing.DirectedWork.Automation.Pages
{
    public static class OperatorZonePage
    {
        internal static Object ul_containerZone = new { Class = "zonebtn" };
        public static Object lbl_zoneHeader = new { TagName = "h2", Text = "LIST OF ZONES FOR THE SITE :: " + Automation.Properties.TestData.Default.site.ToUpper() };

        public static string ChooseZone(string zoneName)
        {

            #region Local Variables
            string result = string.Empty;
            IWebElement elm_ul_containerZone = null;
            List<IWebElement> list = null;
            IWebElement elm_zone = null;

            #endregion

            #region Steps
            try
            {
                elm_ul_containerZone = ul_containerZone.Init();

                list = elm_ul_containerZone.FindElements(By.TagName("input")).ToList();

                elm_zone = list.First(x => x.GetAttribute("value").ProcessString() == zoneName.ProcessString());

                elm_zone.ClickControl();

                WebArchive.WaitForPageLoad();
               

            }
            catch(Exception ex)
            {
                result = "Error :" + ex;
            }
            return result;
            #endregion

        }

    }
}
