﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QA.AutomationLibrary;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;

namespace Ashley.Manufacturing.DirectedWork.Automation.Pages
{
    public static class OperatorWCenterPage
    {
        internal static Object div_containerWorkCenter = new { Id = "slider1_container" };
        public static Object lbl_wCenterHeader = new { TagName = "h2", Text = "LIST OF WORKCENTER FOR THE ZONE :: " + Automation.Properties.TestData.Default.zone };

        public static string ChooseWorkCenter(string workCenterName)
        {
            #region Local Variables
            string result = string.Empty;          
            IWebElement elm_div_containerWorkCenter = null;
            List<IWebElement> list = null;
            IWebElement elm_workCenter = null;

            #endregion

            #region Steps
            try
            {

                elm_div_containerWorkCenter = div_containerWorkCenter.Init();

                list = elm_div_containerWorkCenter.FindElements(By.TagName("input")).ToList();

                elm_workCenter = list.First(x => x.GetAttribute("value").ProcessString() == workCenterName.ProcessString());

                elm_workCenter.ClickControl();

                WebArchive.WaitForPageLoad();
               
            }
            catch(Exception ex)
            {
                result = "Error :" + ex.Message;
            }
            return result;
            #endregion


        }

    }
}
