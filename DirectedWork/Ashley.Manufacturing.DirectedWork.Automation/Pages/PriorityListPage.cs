﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QA.AutomationLibrary;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Support.UI;

namespace Ashley.Manufacturing.DirectedWork.Automation.Pages
{
    public static class PriorityListPage
    {
        internal static Object btn_reqWork = new { Id = "btnReqWrk" };
        internal static Object btn_35WandOff = new { Id = "btnWndOffLbr" };
        internal static Object btn_20SetupTimeOn = new { Id = "btnWndSetUp" };
        internal static Object btn_25SetupTimeOff = new { Id = "btnWndSetupOff" };
        internal static Object btn_50IndirectDtOn = new { Id = "btnDownTimeOn" };
        internal static Object btn_55IndirectDtOff = new { Id = "btnDownTimeOff" };
        internal static Object btn_60AddToGroup = new { Id = "btnAddGroup60" };
        internal static Object btn_65RemoveFromGroup = new { Id = "btnRemoveGroup65" };
        internal static Object btn_wandOnLabour30 = new { Id = "btnWndOnLbr" };
        internal static Object btn_addRowForManualWandOn = new { Id = "btnAddRow" };

        internal static Object txt_enterMO = new { Id = "txtMOFilter" };
        internal static Object btn_searchMO = new { Id = "basic-addon2" };

        internal static Object row_zeroBox = new { Id = "row0box" };

        internal static Object txt_RFID = new { Id = "txtRFID0" };
        internal static Object btn_OK = new { id = "btnupdTag" };

        internal static Object row_zeroBox1 = new { Id = "row0box1" };

        internal static Object spn_GroupId = new { Id = "spnGrp" };

        internal static Object tbl_wandOff = new { Class = "tblWandOFF" };
        internal static Object row_wandOff = new { Id = "0" };
        internal static Object btn_OkWandOff = new { Id = "btnOk" };
        internal static Object btn_OkAtWarning = new { TagName = "Span", Text = "OK" };

        internal static Object txt_quantity = new { Id = "txtQty0" };
        internal static Object drpdwn_scrapCode = new { Id = "ddlScrap0" };
        internal static Object txt_scrapQty = new { Id = "txtScrapQty0" };
        internal static Object btn_okBtn = new { Id = "btnOk" };
        internal static Object drpdwn_plaaetComplete = new { Id = "ddlPLComplete0" };

        internal static Object overlay_Information = new { Class = "ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" };
        internal static Object btn_overlayOk = new { Class = "ui-button-text" , text = "OK"};
        internal static Object btn_overlayCancel = new { Class = "ui-button-text", text = "Cancel" };


        internal static Object drpdown_downTimeReason = new { Id = "ddlDownTimeCode" };
        internal static Object btn_downtimeOk = new { Id = "btnDownok" };
        internal static Object btn_downTimeOff = new { Id = "btnOk" };

        internal static Object txt_addEmpNo = new { Id = "txtEmpname" };
        internal static Object btn_addToGrpOk = new { Id = "btnAddGroupok" };
        internal static Object btn_infoOk = new { TagName = "span", Text = "OK" };
        internal static Object btn_removeFromGroup = new { Id = "btnAddGroupCancel" };
        internal static Object infoAlertForRemoveEmpFromGroup = new { Id = "dvMessage" };

        internal static Object lbl_priorityListHeader = new { TagName = "h2", Class = "header_workCenter header_PriorityList" };


        //Verona Bedding

        internal static Object btn_cancel = new { Id = "btnCancelTag" };
        internal static Object txt_serial = new { Id = "txtSerial" };

        internal static Object row_wandOff01 = new { Id = "0" };
        internal static Object row_wandOff02 = new { Id = "1" };
        internal static Object txt_wandOffQuantity = new { Id = "txtQty0" };

        internal static Object drpdwn_downTime_Code = new { Id = "ddlDownTimeCode" };     

        //internal static Object btn_OK_downTime_Off = new { Id = "btnOk" };
        internal static object tcol_lowerGrid = new { Id = "contenttablebox1" };
        internal static Object tbl_orderSelectedContent = new { Id = "contenttablebox" };
        internal static Object lbl_serialScanning = new { TagName = "h4", Text = "SERIAL SCANNING" };
        internal static Object btn_serialScanCancel = new { TagName = "button", Id = "btnCancelTag", Class = "btn", text = "Cancel" };




        public static string EnterMO(string MO)
        {

            #region Local Varialbles
            string result = string.Empty;
            IWebElement elm_txt_enterMO = null;
            IWebElement elm_btn_searchMO = null;
            IWebElement elm_zeroRow = null;

            #endregion

            #region Steps
            try {
                elm_txt_enterMO = txt_enterMO.Init();
                elm_txt_enterMO.EnterText(MO);

                elm_btn_searchMO = btn_searchMO.Init();
                elm_btn_searchMO.Click();

                elm_zeroRow = row_zeroBox.Init();
                elm_zeroRow.Click();
                WebArchive.WaitForPageLoad();
               

            }
            catch(Exception ex)
            {
                result = "Error Message : " + ex.Message + ex.StackTrace;
            }

            return result;
            #endregion
        }

        public static string SetupTimeOff25(string Qty, string ScrapQty, string ScrapCode)
        {
            #region Local Variables
            string result = string.Empty;
            IWebElement elm_txt_quantity = null;
            IWebElement elm_txt_scrapQty = null;
            IWebElement elm_drpdwn_scrapCode = null;
            IWebElement elm_btn_okBtn = null;

            #endregion

            #region Test Steps
            try
            {
                elm_txt_quantity = txt_quantity.Init();
                elm_txt_quantity.EnterText(Qty);

                elm_txt_scrapQty = txt_scrapQty.Init();
                elm_txt_scrapQty.EnterText(ScrapQty);

                elm_drpdwn_scrapCode = drpdwn_scrapCode.Init();
                var selectElement = new SelectElement(elm_drpdwn_scrapCode);
                selectElement.SelectByIndex(1);

                elm_btn_okBtn = btn_okBtn.Init();
                elm_btn_okBtn.ClickControl();
                


            }
            catch(Exception ex)
            {
                result = "Error Message :" + ex.Message + ex.StackTrace;
            }


            return result;

            #endregion
        }

        public static string WandOff35(string Qty,string ScrapCode,string PalletComplete)
        {
            #region Local Variables
            string result = string.Empty;
            IWebElement elm_txt_quantity = null;
            IWebElement elm_drpdwn_scrapCode = null;
            IWebElement elm_drpdwn_palletComplete = null;
            IWebElement elm_btn_okBtn = null;

            #endregion

            #region Test Steps
            try
            {
                elm_txt_quantity = txt_quantity.Init();
                elm_txt_quantity.EnterText(Qty);

                elm_drpdwn_scrapCode = drpdwn_scrapCode.Init();
                var selectElement = new SelectElement(elm_drpdwn_scrapCode);
                selectElement.SelectByValue(ScrapCode);

                elm_drpdwn_palletComplete = drpdwn_plaaetComplete.Init();
                var selectElement1 = new SelectElement(elm_drpdwn_palletComplete);
                selectElement1.SelectByValue(PalletComplete);

                elm_btn_okBtn = btn_okBtn.Init();
                elm_btn_okBtn.ClickControl();
              


            }
            catch (Exception ex)
            {
                result = "Error Message :" + ex.Message + ex.StackTrace;
            }


            return result;

            #endregion
        }

        public static string AddEmployeeToGroup(string EmpNo)
        {
            #region Local Variables
            string result = string.Empty;
            IWebElement elm_txt_addEmpNo = null;
            IWebElement elm_btn_addToGrpOk = null;
            IWebElement elm_btn_InfoOk = null;
          

            #endregion

            #region Test Steps
            try
            {
                elm_txt_addEmpNo = txt_addEmpNo.Init();
                elm_txt_addEmpNo.EnterText(EmpNo);

                elm_btn_addToGrpOk = btn_addToGrpOk.Init();
                elm_btn_addToGrpOk.Click();
                WebArchive.WaitForPageLoad();

                elm_btn_InfoOk = btn_infoOk.Init();
                elm_btn_InfoOk.ClickControl();
            }

            catch(Exception ex)
            {
                result = "Error Message :" + ex.Message + ex.StackTrace;
            }
            


            #endregion
            return result;

        }

        public static string RemoveEmployeeFromGroup(string EmpNo)
        {
            #region Local Variables
            string result = string.Empty;
            IWebElement elm_txt_removeEmpNo = null;
            IWebElement elm_btn_removeFromGroup = null;
            IWebElement elm_btn_InfoOk = null;
            

            #endregion

            #region Test Steps
            try
            {
                elm_txt_removeEmpNo = txt_addEmpNo.Init();
                elm_txt_removeEmpNo.EnterText(EmpNo);

                elm_btn_removeFromGroup = btn_addToGrpOk.Init();
                elm_btn_removeFromGroup.Click();
                WebArchive.WaitForPageLoad();
                result = infoAlertForRemoveEmpFromGroup.Init().FindElement(By.TagName("td")).Text;
                elm_btn_InfoOk = btn_infoOk.Init();
                elm_btn_InfoOk.ClickControl();
            }

            catch (Exception ex)
            {
                result = "Error Message :" + ex.Message + ex.StackTrace;
            }



            #endregion
            return result;

        }

        public static string DownTimeOnReason(string reason)
        {
            #region Local Variables
            string result = string.Empty;
            IWebElement elm_drpdown_downTimeReason = null;
            IWebElement elm_btn_downtimeOk = null;
            #endregion

            #region Steps
            elm_drpdown_downTimeReason = drpdown_downTimeReason.Init();
            var selectElement = new SelectElement(elm_drpdown_downTimeReason);
            selectElement.SelectByValue(reason);

            elm_btn_downtimeOk = btn_downtimeOk.Init();
            elm_btn_downtimeOk.ClickControl();
            #endregion



            return result;
        }

        public static string EnterVeroneSerial(string Serial)
        {

            #region Local Varialbles
            string result = string.Empty;
            IWebElement elm_txt_serial = null;
            IWebElement elm_btn_OK = null;

            #endregion

            #region Steps
            try
            {
                elm_txt_serial = txt_serial.Init();
                elm_txt_serial.EnterText(Serial);

                elm_btn_OK = PriorityListPage.btn_OK.Init();
                elm_btn_OK.ClickControl();

            }
            catch (Exception ex)
            {
                result = "Error Message : " + ex.Message + ex.StackTrace;
            }

            return result;
            #endregion
        }


        //public static void VerifyInitialAssertion(this IWebDriver driver)
        //{
        //    #region Local Variables
        //    IWebElement elm_reqWork = null;
        //    IWebElement elm_35WandOff = null;
        //    IWebElement elm_20SetupTimeOn = null;
        //    IWebElement elm_25SetupTimeOff = null;
        //    IWebElement elm_50IndirectDtOn = null;
        //    IWebElement elm_55IndirectDtOff = null;
        //    IWebElement elm_65AddToGroup = null;
        //    IWebElement elm_65RemoveFromGroup = null;
        //    IWebElement elm_wandOnLabour30 = null;
        //    IWebElement elm_addRowForManualWandOn = null;
        //    #endregion

        //    #region Steps
        //    elm_reqWork = driver.FindControl(btn_reqWork);
        //    Assert.IsTrue(elm_reqWork.GetAttribute("class").Contains("primary"), "Request Work button is not enabled initially");

        //    elm_35WandOff = driver.FindControl(btn_35WandOff);
        //    if (elm_35WandOff.GetAttribute("class").Contains("primary"))
        //    {
        //        elm_35WandOff.Click();
        //        driver.DisabledWandOff35();
        //    }
        //    else
        //    {
        //        Assert.IsFalse(elm_35WandOff.GetAttribute("class").Contains("primary"), "35 Wand Off button is enabled initially");
        //    }

        //    elm_20SetupTimeOn = driver.FindControl(btn_20SetupTimeOn);
        //    Assert.IsFalse(elm_20SetupTimeOn.GetAttribute("class").Contains("primary"), "20 Setup Time on button is enabled initially");

        //    elm_25SetupTimeOff = driver.FindControl(btn_25SetupTimeOff);
        //    Assert.IsFalse(elm_25SetupTimeOff.GetAttribute("class").Contains("primary"), "25 Setup Time Off button is enabled initially");

        //    elm_50IndirectDtOn = driver.FindControl(btn_50IndirectDtOn);
        //    Assert.IsTrue(elm_50IndirectDtOn.GetAttribute("class").Contains("primary"), "50 Indirect/DT On button is not enabled initially");

        //    elm_55IndirectDtOff = driver.FindControl(btn_55IndirectDtOff);
        //    Assert.IsFalse(elm_55IndirectDtOff.GetAttribute("class").Contains("primary"), "55 Indirect/DT Off button is enabled initially");

        //    elm_65AddToGroup = driver.FindControl(btn_65AddToGroup);
        //    Assert.IsTrue(elm_65AddToGroup.GetAttribute("class").Contains("primary"), "60 Add To Group button is not enabled initially");

        //    elm_65RemoveFromGroup = driver.FindControl(btn_65RemoveFromGroup);
        //    Assert.IsTrue(elm_65RemoveFromGroup.GetAttribute("class").Contains("primary"), "65 Remove From Group button is not enabled initially");

        //    elm_wandOnLabour30 = driver.FindControl(btn_wandOnLabour30);
        //    Assert.IsFalse(elm_wandOnLabour30.GetAttribute("class").Contains("primary"), "Wand On Labour (30) button is enabled initially");

        //    elm_addRowForManualWandOn = driver.FindControl(btn_addRowForManualWandOn);
        //    Assert.IsTrue(elm_addRowForManualWandOn.GetAttribute("class").Contains("primary"), "Add Row for Manual wand on button is not enabled initially");
        //    #endregion
        //}


        //public static void DisabledWandOff35(this IWebDriver driver)
        //{
        //    #region Local Variables
        //    IWebElement elm_tblWandOff = null;
        //    int columnIndex = 0;
        //    IWebElement elm_rowWandOff = null;
        //    IWebElement elm_cell = null;
        //    IWebElement elm_ok = null;
        //    IWebElement elm_okAtWarning = null;
        //    #endregion

        //    #region Steps
        //    elm_tblWandOff = driver.FindControl(tbl_wandOff);

        //    columnIndex = elm_tblWandOff.FindElements(By.TagName("th")).IndexOf(elm_tblWandOff.FindElements(By.TagName("th")).First(x => x.GetText().ProcessString() == "qty"));

        //    elm_rowWandOff = driver.FindElements(By.TagName("tr")).First(x => x.GetAttribute("id") == "0");

        //    elm_cell = elm_tblWandOff.FindElements(By.TagName("td"))[columnIndex];

        //    elm_cell.FindElement(By.TagName("input")).SetText("1");

        //    columnIndex = elm_tblWandOff.FindElements(By.TagName("th")).IndexOf(elm_tblWandOff.FindElements(By.TagName("th")).First(x => x.GetText().ProcessString() == "palletcomplete"));

        //    elm_cell = elm_tblWandOff.FindElements(By.TagName("td"))[columnIndex];

        //    elm_cell.FindElement(By.TagName("select")).ChooseValue("Y");

        //    elm_ok = driver.FindControl(btn_OkWandOff);
        //    elm_ok.Click();

        //    driver.WaitForReady();

        //    elm_okAtWarning = driver.FindControl(btn_OkAtWarning);
        //    elm_okAtWarning.Click();

        //    driver.WaitForReady();
        //    #endregion
        //}

        //public static void EnterMO(this IWebDriver driver, string MO)
        //{

        //    #region Local Varialbles
        //    IWebElement elm_enterMO = null;
        //    IWebElement elm_searchMO = null;
        //    IWebElement elm_zeroRow = null;

        //    #endregion

        //    #region Steps
        //    elm_enterMO = driver.FindControl(txt_enterMO);
        //    elm_enterMO.SetText(MO);

        //    elm_searchMO = driver.FindControl(btn_searchMO);
        //    elm_searchMO.Click();

        //    driver.WaitForReady();
        //    elm_zeroRow = driver.FindControl(row_zeroBox);
        //    elm_zeroRow.Click();

        //    #endregion
        //}

        //// 30 - WandOn
        //public static Object WandOn30(this IWebDriver driver,string RFID)
        //{

        //    #region Local Variables
        //    Object timeOfClicked = string.Empty;
        //    IWebElement elm_wandOnLabour30 = null;
        //    IWebElement elm_txtRFID = null;
        //    IWebElement elm_OK = null;
        //    #endregion

        //    #region Steps
        //    elm_wandOnLabour30 = driver.FindControl(btn_wandOnLabour30);
        //    Assert.IsTrue(elm_wandOnLabour30.Enabled, "Wand On Labour(30) button is not enabled after selecting the order row");
        //    elm_wandOnLabour30.Click();

        //    timeOfClicked = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time"));

        //    driver.WaitForReady();

        //    elm_txtRFID = driver.FindControl(txt_RFID);
        //    elm_txtRFID.SetText(RFID);

        //    elm_OK = driver.FindControl(btn_OK);
        //    elm_OK.Click();
        //    driver.WaitForReady();

        //    return timeOfClicked;
        //    #endregion
        //}

        //// 20 - SetUptime On
        //public static Object SetUpTimeOn20(this IWebDriver driver)
        //{
        //    #region Local Variables
        //    Object timeOfClicked = string.Empty;
        //    IWebElement elm_setUpTimeOn20 = null;          

        //    #endregion

        //    #region Steps
        //    elm_setUpTimeOn20 = driver.FindControl(btn_wandOnLabour30);
        //    Assert.IsTrue(elm_setUpTimeOn20.Enabled, "Wand On Labour(30) button is not enabled after selecting the order row");
        //    elm_setUpTimeOn20.Click();

        //    timeOfClicked = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time"));

        //    driver.WaitForReady();
        //    return timeOfClicked;
        //    #endregion
        //}
        //public static void VerifyAssertionAfterGeneratingOrder(this IWebDriver driver,string groupNumber)
        //{
        //    #region Local Variables
        //    IWebElement elm_reqWork = null;
        //    IWebElement elm_35WandOff = null;
        //    IWebElement elm_20SetupTimeOn = null;
        //    IWebElement elm_25SetupTimeOff = null;
        //    IWebElement elm_50IndirectDtOn = null;
        //    IWebElement elm_55IndirectDtOff = null;
        //    IWebElement elm_65AddToGroup = null;
        //    IWebElement elm_65RemoveFromGroup = null;
        //    IWebElement elm_wandOnLabour30 = null;
        //    IWebElement elm_addRowForManualWandOn = null;
        //    IWebElement elm_group = null;
        //    string groupNumberDisplayed = string.Empty;
        //    #endregion

        //    #region Steps
        //    elm_reqWork = driver.FindControl(btn_reqWork);
        //    Assert.IsTrue(elm_reqWork.GetAttribute("class").Contains("primary"), "Request Work button is not enabled after generating order");

        //    elm_35WandOff = driver.FindControl(btn_35WandOff);
        //    Assert.IsTrue(elm_35WandOff.GetAttribute("class").Contains("primary"), "35 Wand Off button is not enabled after generating order");

        //    elm_20SetupTimeOn = driver.FindControl(btn_20SetupTimeOn);
        //    Assert.IsFalse(elm_20SetupTimeOn.GetAttribute("class").Contains("primary"), "20 Setup Time on button is enabled after generating order");

        //    elm_25SetupTimeOff = driver.FindControl(btn_25SetupTimeOff);
        //    Assert.IsFalse(elm_25SetupTimeOff.GetAttribute("class").Contains("primary"), "25 Setup Time Off button is enabled after generating order");

        //    elm_50IndirectDtOn = driver.FindControl(btn_50IndirectDtOn);
        //    Assert.IsTrue(elm_50IndirectDtOn.GetAttribute("class").Contains("primary"), "50 Indirect/DT On button is not enabled after generating order");

        //    elm_55IndirectDtOff = driver.FindControl(btn_55IndirectDtOff);
        //    Assert.IsFalse(elm_55IndirectDtOff.GetAttribute("class").Contains("primary"), "55 Indirect/DT Off button is enabled after generating order");

        //    elm_65AddToGroup = driver.FindControl(btn_65AddToGroup);
        //    Assert.IsTrue(elm_65AddToGroup.GetAttribute("class").Contains("primary"), "60 Add To Group button is not enabled after generating order");

        //    elm_65RemoveFromGroup = driver.FindControl(btn_65RemoveFromGroup);
        //    Assert.IsTrue(elm_65RemoveFromGroup.GetAttribute("class").Contains("primary"), "65 Remove From Group button is not enabled after generating order");

        //    elm_wandOnLabour30 = driver.FindControl(btn_wandOnLabour30);
        //    Assert.IsFalse(elm_wandOnLabour30.GetAttribute("class").Contains("primary"), "Wand On Labour (30) button is enabled after generating order");

        //    elm_addRowForManualWandOn = driver.FindControl(btn_addRowForManualWandOn);
        //    Assert.IsTrue(elm_addRowForManualWandOn.GetAttribute("class").Contains("primary"), "Add Row for Manual wand on button is not enabled after generating order");

        //    driver.VerifyGreenColorInOrderedTable();

        //    elm_group = driver.FindControl(spn_GroupId);
        //    Assert.IsTrue(elm_group.GetCssValue("animation") == "blinker 1s linear 0s infinite normal none running", "Group Id is not blinking after generating the order");

        //    groupNumberDisplayed = elm_group.GetText().Split('[')[1].Replace("]", "");

        //    Assert.AreEqual(groupNumber, groupNumberDisplayed, "Given Group Number " + groupNumber + " is not matched with displayed group number : " + groupNumberDisplayed);
        //    #endregion

        //}

        //public static void VerifyAssertionAfter20(this IWebDriver driver)
        //{
        //    #region Local Variables
        //    IWebElement elm_reqWork = null;
        //    IWebElement elm_35WandOff = null;
        //    IWebElement elm_20SetupTimeOn = null;
        //    IWebElement elm_25SetupTimeOff = null;
        //    IWebElement elm_50IndirectDtOn = null;
        //    IWebElement elm_55IndirectDtOff = null;          
        //    IWebElement elm_wandOnLabour30 = null;           
        //    IWebElement elm_group = null;

        //    #endregion

        //    #region Steps
        //    elm_reqWork = driver.FindControl(btn_reqWork);
        //    Assert.IsTrue(elm_reqWork.GetAttribute("class").Contains("primary"), "Request Work button is not enabled after generating order");

        //    elm_25SetupTimeOff = driver.FindControl(btn_25SetupTimeOff);
        //    Assert.IsFalse(elm_25SetupTimeOff.GetAttribute("class").Contains("primary"), "25 Setup Time Off button is enabled after generating order");

        //    elm_50IndirectDtOn = driver.FindControl(btn_50IndirectDtOn);
        //    Assert.IsTrue(elm_50IndirectDtOn.GetAttribute("class").Contains("primary"), "50 Indirect/DT On button is not enabled after generating order");

        //    elm_35WandOff = driver.FindControl(btn_35WandOff);
        //    Assert.IsTrue(!elm_35WandOff.GetAttribute("class").Contains("primary"), "35 Wand Off button is not enabled after generating order");

        //    elm_wandOnLabour30 = driver.FindControl(btn_wandOnLabour30);
        //    Assert.IsFalse(!elm_wandOnLabour30.GetAttribute("class").Contains("primary"), "Wand On Labour (30) button is enabled after generating order");

        //    elm_20SetupTimeOn = driver.FindControl(btn_20SetupTimeOn);
        //    Assert.IsFalse(!elm_20SetupTimeOn.GetAttribute("class").Contains("primary"), "20 Setup Time on button is enabled after generating order");

        //    elm_55IndirectDtOff = driver.FindControl(btn_55IndirectDtOff);
        //    Assert.IsFalse(!elm_55IndirectDtOff.GetAttribute("class").Contains("primary"), "55 Indirect/DT Off button is enabled after generating order");

        //    elm_group = driver.FindControl(spn_GroupId);
        //    Assert.IsTrue(elm_group.GetCssValue("animation") == "blinker 1s linear 0s infinite normal none running", "Group Id is not blinking after generating the order");


        //    #endregion

        //}
        //public static void VerifyGreenColorInOrderedTable(this IWebDriver driver)
        //{

        //    #region Local Variables
        //    IWebElement elm_zeroRowBox1 = null;
        //    List<IWebElement> list_cells = null;
        //    #endregion


        //    #region Steps
        //    elm_zeroRowBox1 = driver.FindControl(row_zeroBox1);

        //    list_cells = elm_zeroRowBox1.GetAllVisibileChildren();

        //    list_cells.ForEach(x => Assert.IsTrue(x.GetAttribute("class").Contains("green"),"After generating order , cell style-color is not changed to Green"));
        //    #endregion
        //}


    }
}
