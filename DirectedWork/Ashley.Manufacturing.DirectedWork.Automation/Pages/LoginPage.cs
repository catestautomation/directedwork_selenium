﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QA.AutomationLibrary;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Ashley.Manufacturing.DirectedWork.Automation.Pages
{

    
    public static class LoginPage
    {
        internal static Object txt_userName = new { id = "UserName" };
        internal static Object txt_password = new { id = "Password" };
        internal static Object btn_login = new { Class = "loginBtn" };
    }
}
