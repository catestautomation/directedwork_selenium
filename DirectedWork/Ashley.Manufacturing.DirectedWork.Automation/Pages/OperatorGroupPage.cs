﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QA.AutomationLibrary;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Ashley.Manufacturing.DirectedWork.Automation.Pages
{
    public static class OperatorGroupPage
    {
        internal static Object div_containerOperatorGroup = new { Id = "responsive" };

        internal static Object cmb_group = new { Id = "ulGrpLst" };

        internal static Object btn_OK = new { Id = "saveEdit" };
        public static Object lbl_groupHeader = new { TagName = "h4", Text = "Select Group" };

        public static string ChooseOperatorGroup(string operatorGroupName)
        {

            #region Local Variables
            string result = string.Empty;
            IWebElement elm_div_containerOperatorGroup = null;
            IWebElement elm_operatorGroup = null;
            IWebElement elm_btn_OK = null;

            #endregion

            #region Steps
            try
            {
                elm_div_containerOperatorGroup = div_containerOperatorGroup.Init();

                elm_operatorGroup = elm_div_containerOperatorGroup.FindElement(By.TagName("select"));

                var selectElement = new SelectElement(elm_operatorGroup);
                selectElement.SelectByText(operatorGroupName);

             //   elm_operatorGroup.SelectDropDownValueByText(operatorGroupName);

                elm_btn_OK = btn_OK.Init();
                elm_btn_OK.ClickControl();

                WebArchive.WaitForPageLoad();
               

            }
            catch(Exception ex)
            {
                result = "Error :" + ex.Message;
            }

            return result;

            #endregion

        }
    }
}


