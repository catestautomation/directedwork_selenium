﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QA.AutomationLibrary;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;

namespace Ashley.Manufacturing.DirectedWork.Automation.Pages
{
    public static class OperatorSitesPage
    {
        internal static Object div_container = new { Id = "slider1_container" };
        internal static Object lst_operatorSite = new { TagName = "input" };
        public static Object lbl_siteHeader = new { TagName = "h2", Id = "hdrWrokCenter", Text = "LIST OF SITES" };

        public static string ChooseOperatorSite(string operatorSiteName)
        {

            #region Local Variables

            IWebElement elm_div_container = null;
            List<IWebElement> list = null;
            IWebElement elm_operatorSite = null;
            string result = string.Empty;

            #endregion

            #region Steps
            try
            {
                elm_div_container = div_container.Init();

              //  list = elm_div_container.GetAllVisibileChildren();

                list = elm_div_container.FindElements(By.TagName("input")).ToList();

                elm_operatorSite = list.First(x => x.GetAttribute("value").ProcessString() == operatorSiteName.ProcessString());

                elm_operatorSite.Focus();

                elm_operatorSite.ClickControl();

                WebArchive.WaitForPageLoad();
               

            }
            catch(Exception ex)
            {
                result = "Error : " + ex;
            }
            return result;
            #endregion

        }
    }
}
